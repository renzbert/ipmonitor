package model;

import controller.NtpTime;
import controller.Utility;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.peers.Peer;
import org.gudy.azureus2.plugins.peers.PeerDescriptor;
import org.gudy.azureus2.plugins.peers.PeerEvent;
import org.gudy.azureus2.plugins.peers.PeerListener2;
import org.gudy.azureus2.plugins.torrent.Torrent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class ObservedPeer extends ModelObject {
    /**
     * The {@link Peer} this object is referring.
     */
    private Peer peer;
    /**
     * The IP address of the observed peer.
     */
    private String ip;
    /**
     * The port of the observed peer.
     */
    private int port;
    /**
     * *The id of the observed peer.
     */
    private byte[] id;
    /**
     * *The used BitTorrent client of the observed peer.
     */
    private String client = "";
    /**
     * *The the current state of the observed peer.
     */
    private String state;
    /**
     * *The country code according to a GeoIP service.
     */
    private String countryCode = "";
    /**
     * The {@link PeerDescriptor} delivered by the peer discovery mechanism
     */
    private PeerDescriptor pd;
    /**
     * Indicates if the observed peer uses the Azureus messaging protocol
     */
    private boolean az = false;
    /**
     * The {@link Download} this peer is referring to.
     */
    private Download download;
    /**
     * The {@link Torrent} this peer is referring to.
     */
    private Torrent torrent;
    /**
     * Date of the first occurrence of this observed peer
     */
    private Date date;
    /**
     * Indicates if this peer is selected as target.
     */
    private boolean target;
    /**
     * List of ingoing messages.
     */
    private List<InterceptedMessage> inMsgList;
    /**
     * List of outgoing messages.
     */
    private List<InterceptedMessage> outMsgList;
    /**
     * Count of pieces downloaded from this peer, indicated via have messages.
     */
    private int pieceCount = 0;

    public ObservedPeer(PeerDescriptor pd, Download d) {
        this.pd = pd;
        download = d;
        torrent = d.getTorrent();
        date = new Date(NtpTime.getInstance().now());
        target = false;
        ip = pd.getIP();
        port = pd.getTCPPort();
        state = "DISCOVERED";
        setCountryCode();
        inMsgList = new ArrayList<>();
        outMsgList = new ArrayList<>();
    }

    public PeerDescriptor getPd() {
        return pd;
    }

    public void setPd(PeerDescriptor pd) {
        this.pd = pd;
    }

    public boolean isTarget() {
        return target;
    }

    public void setTarget(boolean isTarget) {
        firePropertyChange("target", this.target, this.target = isTarget);
    }

    public void setInMsgList(List<InterceptedMessage> msgList) {
        this.inMsgList = msgList;
        firePropertyChange("inMsgList", null, msgList);
    }

    /**
     * Adds an intercepted message to the inMsgList.
     *
     * @param message
     */
    public void addInMessage(InterceptedMessage message) {
        inMsgList.add(message);
        firePropertyChange("inMsgList", null, inMsgList);
    }

    public List<InterceptedMessage> getInMsgList() {
        return inMsgList;
    }

    public int getPieceCount() {
        return pieceCount;
    }

    public void setOutMsgList(List<InterceptedMessage> msgList) {
        this.outMsgList = msgList;
        firePropertyChange("outMsgList", null, msgList);
    }

    /**
     * Adds an intercepted message to the outMsgList.
     * In addition this method detects complete pieces downloaded from this peer for the pieceCount.
     *
     * @param message
     */
    public void addOutMessage(InterceptedMessage message) {
        outMsgList.add(message);
        if (message.getMessage().getID().equals("BT_HAVE")) {
            long pieceSize = torrent.getPieceSize();
            long received = getPeer().getStats().getTotalReceived();
            int newPieceCnt = (int) (received / pieceSize);
            firePropertyChange("pieceCount", pieceCount, pieceCount = newPieceCnt);
        }
        firePropertyChange("outMsgList", null, outMsgList);
    }

    public List<InterceptedMessage> getOutMsgList() {
        return outMsgList;
    }

    public byte[] getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public Download getDownlaod() {
        return download;
    }

    public Torrent getTorrent() {
        return torrent;
    }

    public Date getDate() {
        return date;
    }

    public Peer getPeer() {
        return peer;
    }

    public void setPeer(Peer p) {
        firePropertyChange("peer", peer, peer = p);
        peer.addListener(new PeerListener2() {
            @Override
            public void eventOccurred(PeerEvent event) {
                update();
            }
        });
        update();
    }

    public String getIp() {
        return ip;
    }

    public String getState() {
        return state;
    }

    public int getPort() {
        return port;
    }

    private void update() {
        firePropertyChange("ip", ip, ip = peer.getIp());
        firePropertyChange("id", id, id = peer.getId());
        firePropertyChange("client", client, client = peer.getClient());
        firePropertyChange("state", state, state = Utility.stateToString(peer.getState()));
        firePropertyChange("port", port, port = peer.getPort());
        firePropertyChange("countryCode", countryCode, countryCode = Utility.getCountryLocator().getIPISO3166(ip));

    }

    public String getCountryCode() {
        return countryCode;
    }

    private void setCountryCode() {
        firePropertyChange("countryCode", countryCode, countryCode = Utility.getCountryLocator().getIPISO3166(ip));
    }

    public void setAz(boolean b) {
        firePropertyChange("az", az, az = b);
    }

    public boolean isAz() {
        return az;
    }

}
