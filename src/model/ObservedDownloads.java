package model;

import org.gudy.azureus2.plugins.download.Download;

import java.util.ArrayList;
import java.util.List;

/**
 * Aggregation of {@link ObservedDownload} objects for GUI.
 */
public class ObservedDownloads extends ModelObject {
    /**
     * List of {@link ObservedDownload}s.
     */
    private List<ObservedDownload> observedDownloads;

    public ObservedDownloads() {
        observedDownloads = new ArrayList<>();
    }

    /**
     * Adds an {@link ObservedDownload} to  observedDownloads.
     *
     * @param download
     */
    public void addDownload(ObservedDownload download) {
        observedDownloads.add(download);
        firePropertyChange("observedDownloads", null, observedDownloads);
    }

    /**
     * Removes an {@link ObservedDownload} of  observedDownloads.
     *
     * @param download
     */
    public void removeDownload(ObservedDownload download) {
        observedDownloads.remove(download);
        firePropertyChange("observedDownloads", null, observedDownloads);
    }

    /**
     * Gets an {@link ObservedDownload} by a {@link Download}
     *
     * @param download
     * @return the referring {@link ObservedDownload}, null if not present
     */
    public ObservedDownload getDownload(Download download) {
        for (ObservedDownload observedDownload : observedDownloads) {
            if (observedDownload.getDownload() == download) {
                return observedDownload;
            }
        }
        return null;
    }

    /**
     * Checks if the given {@link Download} is present in the observedDownloads list.
     *
     * @param download
     * @return true if the {@link Download} is present, otherwise false
     */
    public boolean containsDownload(Download download) {
        for (ObservedDownload observedDownload : observedDownloads) {
            if (observedDownload.getDownload() == download) {
                return true;
            }
        }
        return false;
    }

    public List<ObservedDownload> getObservedDownloads() {
        return observedDownloads;
    }
}
