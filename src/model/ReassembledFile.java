package model;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * This class represents a download file which content is downloaded and reassembled by the ipmonitor plugin.
 */
public class ReassembledFile extends ModelObject {
    /**
     * The name of the ReassembledFile.
     */
    private final String name;
    /**
     * The path of the ReassembledFile.
     */
    private final String path;
    /**
     * The actual {@link File}.
     */
    private File file;
    /**
     * The file size specified in the corresponding torrent file.
     */
    private final long size;
    /**
     * The amount of pieces of this files.
     */
    private final long pieces;
    /**
     * The current count of processed pieces.
     */
    private long pieceCnt = 0;
    /**
     * The time a part of this file appears first.
     */
    private long startTime = 0;
    /**
     * The time a part of this file appears last.
     */
    private long endTime = 0;
    /**
     * Indicates if this file is completely reassembled.
     */
    private boolean complete = false;

    public ReassembledFile(String name, long size, long pieces, String path) {
        this.name = name;
        this.size = size;
        this.pieces = pieces;
        this.path = path;
        file = new File(path + System.getProperty("file.separator") + name);
    }

    /**
     * Adds data to the file. As in BitTorrent data comes in pieces(chunks of the file) distributed over the file,
     * it has to be copied to the right offset inside the file.
     *
     * @param offset    starting point of new data
     * @param length    the length of the data to copy
     * @param data      array containing the data
     * @param startTime start timestamp of the data to copy
     * @param endTime   end timestamp of the data to copy
     */
    public void addData(long offset, long length, byte[] data, long startTime,
                        long endTime) throws IOException {
        updateTime(startTime, endTime);
        File targetFile = new File(path + System.getProperty("file.separator") + name);
        File parent = targetFile.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }


        RandomAccessFile raf = new RandomAccessFile(targetFile, "rw");
        raf.setLength(this.getSize());
        raf.seek(offset);
        raf.write(data, 0, (int) length);
        raf.close();
        pieceCnt++;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getName() {
        return name;
    }

    public long getSize() {
        return size;
    }

    public long getPieces() {
        return pieces;
    }

    public long getPieceCnt() {
        return pieceCnt;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    private void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    private void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    private void updateTime(long start, long end) {
        if (startTime == 0) {
            setStartTime(Math.min(start, end));
        }
        if (endTime == 0) {
            setEndTime(Math.max(start, end));
        }
        if (start < startTime || startTime == 0) {
            setStartTime(start);
        }
        if (end > endTime || endTime == 0) {
            setEndTime(end);
        }
    }
}
