package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Base class which provides property change support for POJOs
 */
public class ModelObject {
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(
            this);

    /**
     * Adds {@link PropertyChangeListener)
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Removes {@link PropertyChangeListener}
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Adds {@link PropertyChangeListener) by propertyName
     *
     * @param propertyName
     * @param listener
     */
    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Removes {@link PropertyChangeListener} by propertyName
     *
     * @param propertyName
     * @param listener
     */
    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Fire generic propertyChangeEvent
     *
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    protected void firePropertyChange(String propertyName, Object oldValue,
                                      Object newValue) {
        changeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    /**
     * Fire boolean propertyChangeEvent
     *
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    protected void firePropertyChange(String propertyName, boolean oldValue,
                                      boolean newValue) {
        changeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }
}