package model;

import com.aelitis.azureus.core.peermanager.messaging.bittorrent.ltep.LTMessage;
import org.gudy.azureus2.plugins.messaging.Message;

import java.nio.ByteBuffer;

/**
 * This class represents a single message which was intercepted by the ipmonitor plugin.
 */
public class InterceptedMessage {
    /**
     * The referring {@link Message} of Vuze.
     */
    private final Message message;
    /**
     * Timestamp of sending/receiving the message.
     */
    private final long time;
    /**
     * Copy of the {@link ByteBuffer} of the {@link Message}.
     * <p>
     * Can be null if the type of the message do not contain any payload data.
     */
    private byte[] data;

    public InterceptedMessage(Message message, long time) {
        this.message = message;
        this.time = time;
        extractData(message);
    }

    /**
     * Extracts the payload data of the message
     * @param message
     */
    private synchronized void extractData(Message message) {
        try {
            ByteBuffer buffer;
            if (!message.getID().equals(LTMessage.ID_LT_HANDSHAKE)) {
                ByteBuffer[] payload = message.getPayload();
                if (payload.length == 2 && message.getID().equals("BT_PIECE")) {
                    buffer = payload[1];
                    copyData(buffer, 0);
                } else if (message.getID().equals("BT_BITFIELD")) {
                    buffer = payload[0];
                    if (buffer != null) {
                        copyData(buffer, 0);
                    }
                } else {
                    if (payload.length > 0) {
                        buffer = message.getPayload()[0];
                        copyData(buffer, 0);
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void copyData(ByteBuffer buffer, int startPosition) {
        int position = buffer.position();
        int limit = buffer.limit();
        int metadata = 0;
        if (startPosition < 0) {
            metadata = buffer.position();
        } else {
            metadata = startPosition;
        }
        data = new byte[limit - metadata];
        buffer.position(metadata);// Cut of MetaData just in Case position != 9
        buffer.get(data);
        buffer.position(position);
        buffer.limit(limit);
    }

    public byte[] getData() {
        return data;
    }

    public Message getMessage() {
        return message;
    }

    public long getTime() {
        return time;
    }

}
