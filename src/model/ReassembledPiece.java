package model;

/**
 * This class represents a download piece which content is downloaded and reassembled by the ipmonitor plugin.
 */
public class ReassembledPiece extends ModelObject implements Comparable<ReassembledPiece> {
    /**
     * Index of the piece inside the torrent.
     */
    private final long index;
    /**
     * Start timestamp of this piece.
     */
    private long startTime;
    /**
     * End timestamp of this piece.
     */
    private long endTime;
    /**
     * Length of the data array.
     */
    private final long length;
    /**
     * Array containing the piece data.
     */
    private byte[] data;
    /**
     * Indicated whether this piece is complete reassembled.
     */
    private boolean complete = false;
    /**
     * Bitfield array tracking the presence of the blocks of this piece.
     */
    private int[] blocks;

    public ReassembledPiece(long key, long length, long startTime) {
        this.index = key;
        this.startTime = startTime;
        this.endTime = startTime;
        this.length = length;
        data = new byte[(int) length];
        blocks = new int[(int) length];
        for (int i = 0; i < blocks.length; i++) {
            blocks[i] = 0;
            data[i] = 0;
        }
    }

    /**
     * @param time
     * @param offset
     * @param buffer
     */
    public void addData(long time, long offset, byte[] buffer) {
        updateTime(time);
        for (int i = (int) offset; i < offset + buffer.length && i < length; i++) {
            try {
                blocks[i] = 1;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            int copyLength;
            if ((offset + buffer.length) > length) {
                copyLength = (int) ((offset + buffer.length) - length);
            } else {
                copyLength = buffer.length;
            }
            System.arraycopy(buffer, 0, data, (int) (offset), copyLength);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTime(long time) {
        if (time > startTime) {
            firePropertyChange("startTime", startTime, startTime = time);
        } else if (time < endTime) {
            firePropertyChange("endTime", endTime, endTime = time);
        }
    }

    /**
     * Computes the completeness of the piece and returns it.
     *
     * @return the completeness of the piece
     */
    public boolean isComplete() {
        complete = true;
        for (int i = 0; i < blocks.length; i++) {
            if (blocks[i] == 0) {
                complete = false;
                return complete;
            }
        }
        return complete;
    }

    public long getIndex() {
        return index;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public byte[] getData() {
        return data;
    }

    public long getLength() {
        return length;
    }

    /**
     * Compares a piece ascending by its index.
     *
     * @param reassembledPiece
     * @return index difference of the two pieces
     */
    @Override
    public int compareTo(ReassembledPiece reassembledPiece) {
        return (int) (this.getIndex() - reassembledPiece.getIndex());
    }

}
