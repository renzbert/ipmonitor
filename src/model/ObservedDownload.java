package model;

import controller.TorCheck;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.peers.PeerDescriptor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a observed download inside the ipmonitor Vuze plugin, the downloading torrent.
 * It handles the peer management including the peer filtering.
 */
public class ObservedDownload extends ModelObject {
    /**
     * This value contains the referring {@link Download} instance in Vuze.
     */
    private Download download;
    /**
     * The name of the torrent.
     */
    private String name;
    /**
     * List of the observed peers of this download.
     */
    private List<ObservedPeer> observedPeers = new ArrayList<>();

    /**
     * List of filter elements of white list filter.
     */
    private List<String> whiteListFilter = new ArrayList<>();
    /**
     * List of filter elements of black list filter.
     */
    private List<String> blackListFilter = new ArrayList<>();
    /**
     * List of filter elements of country filter.
     */
    private List<String> countryFilter = new ArrayList<>();

    /**
     * This value indicated if the the white list filter is active.
     */
    private boolean white;
    /**
     * This value indicated if the the black list filter is active.
     */
    private boolean black;
    /**
     * This value indicated if the the country filter is active.
     */
    private boolean country;

    public List<String> getWhiteListFilter() {
        return whiteListFilter;
    }

    public void setWhiteListFilter(List<String> whiteListFilter) {
        firePropertyChange("whiteListFilter", this.whiteListFilter, this.whiteListFilter = whiteListFilter);
    }

    public List<String> getBlackListFilter() {
        return blackListFilter;
    }

    public void setBlackListFilter(List<String> blackListFilter) {
        firePropertyChange("blackListFilter", this.blackListFilter, this.blackListFilter = blackListFilter);
    }

    public List<String> getCountryFilter() {
        return countryFilter;
    }

    public void setCountryFilter(List<String> countryFilter) {
        firePropertyChange("countryFilter", this.countryFilter, this.countryFilter = countryFilter);
    }

    /**
     * Constructs a new ObservedDownload
     *
     * @param download        the {@link Download} of Vuze
     * @param pluginInterface the Vuze plugin API
     */
    public ObservedDownload(Download download, PluginInterface pluginInterface) {
        this.download = download;
        name = download.getName();

        //Exclude loop back address
        blackListFilter.add("127.0.0.1");

        white = pluginInterface.getPluginconfig().getPluginBooleanParameter("whiteListDefaultEnabled");
        black = pluginInterface.getPluginconfig().getPluginBooleanParameter("blackListDefaultEnabled");
        country = pluginInterface.getPluginconfig().getPluginBooleanParameter("countryDefaultEnabled");

        String pathCountry = pluginInterface.getPluginconfig().getPluginStringParameter("countryFilter");
        initFilter(pathCountry, countryFilter);
        String pathWhiteList = pluginInterface.getPluginconfig().getPluginStringParameter("whitelistFilter");
        initFilter(pathWhiteList, whiteListFilter);
        String pathBlackList = pluginInterface.getPluginconfig().getPluginStringParameter("blacklistFilter");
        initFilter(pathBlackList, blackListFilter);
    }

    private void initFilter(String path, List<String> list) {
        String line;
        try {
            InputStream fis = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an {@link ObservedPeer} of a {@link PeerDescriptor} provided by a peer source and adds it to the observedPeers.
     *
     * @param peerDescriptor
     * @return the created and added {@link ObservedPeer}
     */
    public ObservedPeer addPeer(PeerDescriptor peerDescriptor) {
        ObservedPeer peer = new ObservedPeer(peerDescriptor, download);
        peer.addPropertyChangeListener("target", new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                firePropertyChange("observedPeers", null, observedPeers);
            }
        });
        observedPeers.add(peer);
        firePropertyChange("observedPeers", null, observedPeers);
        return peer;
    }

    /**
     * Removes all peers of observedPeers.
     */
    public void removeAllPeers() {
        observedPeers = new ArrayList<>();
        firePropertyChange("observedPeers", null, observedPeers);
    }

    /**
     * Gets a {@link ObservedPeer} by ip address.
     *
     * @param ip search ip address
     * @return the peer with matching ip address, null if not present
     */
    public ObservedPeer getPeerByIp(String ip) {
        for (ObservedPeer peer : observedPeers) {
            if (peer.getIp().equals(ip)) {
                return peer;
            }
        }
        return null;
    }

    public void setDownload(Download download) {
        firePropertyChange("download", this.download, this.download = download);
        if (!download.getName().equals(name)) {
            firePropertyChange("name", name, name = download.getName());
        }

    }

    public Download getDownload() {
        return download;
    }

    public String getName() {
        return name;
    }

    public List<ObservedPeer> getObservedPeers() {
        return observedPeers;
    }

    /**
     * This method returns true if the observedPeer passes all three steps of the filering.
     * <p>
     * Step     Filter             Description
     * ----     ------             -----------
     * 0        Tor filter         enabled by parameter
     * 1        White list filter  passed if observedPeer matches
     * 2        Black list filter  passed if observedPeer not matches
     * 3        Country filter     passed if observedPeer's location matches
     *
     * @param observedPeer
     * @param tor          enables the Tor filter
     * @return true if the observedPeer passes all filter steps
     */
    public boolean filter(ObservedPeer observedPeer, boolean tor) {
        if (tor) {
            if (TorCheck.getInstance().getTorExitList().contains(observedPeer.getIp())) {
                return false;
            }
        }
        if (white && whiteListFilter.size() > 0) {
            if (whiteListFilter.contains(observedPeer.getIp())) {
                return true;
            }
        } else {
            if (black) {
                if (blackListFilter.contains(observedPeer.getIp())) {
                    return false;
                }
            }
            if (country) {
                if (countryFilter.contains(observedPeer.getCountryCode())) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean isWhite() {
        return white;
    }

    public void setWhite(boolean isWhite) {
        firePropertyChange("white", white, white = isWhite);
    }

    public boolean isBlack() {
        return black;
    }

    public void setBlack(boolean isBlack) {
        firePropertyChange("black", black, black = isBlack);
    }

    public boolean isCountry() {
        return country;
    }

    public void setCountry(boolean isCountry) {
        firePropertyChange("country", country, country = isCountry);
    }

}
