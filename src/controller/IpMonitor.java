package controller;

import model.ObservedDownloads;
import org.gudy.azureus2.plugins.PluginInterface;

/**
 * Central controller class of the ipmonitor plugin
 */
public class IpMonitor {

    private PeerFetcher peerFetcher;
    private PluginInterface pluginInterface;
    private ObservedDownloads observedDownloads;

    public IpMonitor(PluginInterface pluginInterface) {
        this.pluginInterface = pluginInterface;
        observedDownloads = new ObservedDownloads();
        peerFetcher = new PeerFetcher(pluginInterface, observedDownloads);
        peerFetcher.start();
    }

    public PeerFetcher getPeerFetcher() {
        return peerFetcher;
    }

    public void setPeerFetcher(PeerFetcher peerFetcher) {
        this.peerFetcher = peerFetcher;
    }

    public ObservedDownloads getObservedDownloads() {
        return observedDownloads;
    }

    public void setObservedDownloads(ObservedDownloads observedDownloads) {
        this.observedDownloads = observedDownloads;
    }

    public void initTime(String server) {
        NtpTime.getInstance(server); // Init SysTime
    }

}
