package controller;

import model.InterceptedMessage;
import model.ObservedDownload;
import model.ObservedDownloads;
import model.ObservedPeer;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.download.DownloadManager;
import org.gudy.azureus2.plugins.download.DownloadManagerListener;
import org.gudy.azureus2.plugins.download.DownloadPeerListener;
import org.gudy.azureus2.plugins.messaging.Message;
import org.gudy.azureus2.plugins.network.IncomingMessageQueue;
import org.gudy.azureus2.plugins.network.IncomingMessageQueueListener;
import org.gudy.azureus2.plugins.network.OutgoingMessageQueue;
import org.gudy.azureus2.plugins.network.OutgoingMessageQueueListener;
import org.gudy.azureus2.plugins.peers.PeerDescriptor;
import org.gudy.azureus2.plugins.peers.PeerManager;
import org.gudy.azureus2.plugins.peers.PeerManagerEvent;
import org.gudy.azureus2.plugins.peers.PeerManagerListener2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * The PeerFetcher is the connection between the plugin and the Vuze core.
 * It manages the listeners for the peers of each download.
 */
public class PeerFetcher {

    /**
     * The observed downloads.
     */
    ObservedDownloads observedDownloads;

    public ObservedDownloads getObservedDownloads() {
        return observedDownloads;
    }

    public void setObservedDownloads(ObservedDownloads d) {
        this.observedDownloads = d;
    }

    private PluginInterface pluginInterface;
    private DownloadManager downloadManager;
    private DownloadManagerListener downloadManagerListener;

    public PeerFetcher(PluginInterface pluginInterface, ObservedDownloads observedDownloads) {
        this.pluginInterface = pluginInterface;

        downloadManager = pluginInterface.getDownloadManager();
        this.observedDownloads = observedDownloads;
    }

    /**
     * Starts the peer fetching of all available downloads.
     */
    public void start() {
        downloadManagerListener = new MyDownloadManagerListener();

        downloadManager.addListener(downloadManagerListener);
    }

    /**
     * Stops the peer fetching of all available downloads.
     */
    public void stop() {
        downloadManager.removeListener(downloadManagerListener);
    }

    private class MyDownloadManagerListener implements DownloadManagerListener {

        DownloadPeerListener downloadPeerListener;

        public MyDownloadManagerListener() {
            downloadPeerListener = new MyDownloadPeerListener();

            Download[] downloads = downloadManager.getDownloads();
            for (Download download : downloads) {
                if (!observedDownloads.containsDownload(download)) {
                    addDownload(download);
                }
            }
        }

        private void addDownload(Download download) {
            ObservedDownload observedDownload = new ObservedDownload(download, pluginInterface);
            observedDownloads.addDownload(observedDownload);

            observedDownload.addPropertyChangeListener("countryFilter", propertyChangeEvent -> {
                for (ObservedPeer observedPeer : observedDownload.getObservedPeers()) {
                    if (!observedDownload.getBlackListFilter().contains(observedPeer.getIp())) {
                        if (observedDownload.getCountryFilter().contains(observedPeer.getCountryCode())) {
                            observedPeer.setTarget(true);
                        }
                    }
                }

            });
        }

        @Override
        public void downloadRemoved(Download download) {

        }

        @Override
        public void downloadAdded(Download download) {
            download.addRateLimiter(pluginInterface.getConnectionManager().createRateLimiter("blocktraffic", 0), true);
            download.addPeerListener(downloadPeerListener);
            if (!observedDownloads.containsDownload(download)) {
                addDownload(download);
            }

        }
    }

    private class MyDownloadPeerListener implements DownloadPeerListener {

        @Override
        public void peerManagerAdded(final Download download, final PeerManager peerManager) {
            peerManager.addListener(new PeerManagerListener2() {

                NtpTime ntpTime = NtpTime.getInstance();

                @Override
                public void eventOccurred(PeerManagerEvent event) {

                    ObservedPeer observedPeer;

                    switch (event.getType()) {
                        case PeerManagerEvent.ET_PEER_ADDED:
                            observedPeer = observedDownloads.getDownload(download).getPeerByIp(event.getPeer().getIp());

                            if (observedDownloads.containsDownload(download) && observedPeer != null && !observedPeer.isTarget()) {
                                peerManager.removePeer(event.getPeer());
                            } else if (observedPeer != null && observedPeer.isTarget()) {
                                observedPeer.setPeer(event.getPeer());
                                OutgoingMessageQueue outgoingMessageQueue = observedPeer.getPeer().getConnection().getOutgoingMessageQueue();
                                outgoingMessageQueue.registerListener(new OutgoingMessageQueueListener() {

                                    @Override
                                    public void messageSent(Message message) {
                                        observedPeer.addOutMessage(new InterceptedMessage(message, ntpTime.now()));
                                    }

                                    @Override
                                    public boolean messageAdded(Message message) {
                                        return !message.getID().equals("BT_PIECE");
                                    }

                                    @Override
                                    public void bytesSent(int arg0) {
                                    }
                                });

                                IncomingMessageQueue incomingMessageQueue = observedPeer.getPeer().getConnection().getIncomingMessageQueue();
                                incomingMessageQueue.registerListener(new IncomingMessageQueueListener() {

                                    @Override
                                    public boolean messageReceived(Message message) {
                                        if (message.getID().equals("AZ_HANDSHAKE")) {
                                            observedPeer.setAz(true);
                                        }
                                        observedPeer.addInMessage(new InterceptedMessage(message, ntpTime.now()));
                                        return false;
                                    }

                                    @Override
                                    public void bytesReceived(int arg0) {
                                    }
                                });
                            } else {
                                peerManager.removePeer(event.getPeer());

                            }
                            break;
                        case PeerManagerEvent.ET_PEER_DISCOVERED:
                            if (observedDownloads.containsDownload(download) && observedDownloads.getDownload(download)
                                    .getPeerByIp(event.getPeerDescriptor().getIP()) == null) {

                                observedPeer = observedDownloads.getDownload(download).addPeer(event.getPeerDescriptor());
                                observedPeer.addPropertyChangeListener("target", new PropertyChangeListener() {

                                    @Override
                                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                                        if ((Boolean) propertyChangeEvent.getNewValue()) {
                                            PeerDescriptor peerDescriptor = observedPeer.getPd();
                                            peerManager.addPeer(peerDescriptor.getIP(), peerDescriptor.getTCPPort(), peerDescriptor.getUDPPort(),
                                                    peerDescriptor.useCrypto());
                                        }
                                    }
                                });
                                ObservedDownload observedDownload = observedDownloads.getDownload(download);
                                observedPeer.setTarget(observedDownload.filter(observedPeer, pluginInterface.getPluginconfig().getPluginBooleanParameter("enableTor")));
                            }
                            break;
                        case PeerManagerEvent.ET_PEER_REMOVED:
                            observedPeer = observedDownloads.getDownload(download).getPeerByIp(event.getPeer().getIp());
                            if (observedPeer != null && observedPeer.isTarget()) {
                                PeerDescriptor peerDescriptor = observedPeer.getPd();
                                peerManager.addPeer(peerDescriptor.getIP(), peerDescriptor.getTCPPort(), peerDescriptor.getUDPPort(), peerDescriptor.useCrypto());
                            }
                            break;
                        case PeerManagerEvent.ET_PEER_SENT_BAD_DATA:
                            break;
                        case PeerManagerEvent.ET_PIECE_ACTIVATED:
                            break;
                        case PeerManagerEvent.ET_PIECE_COMPLETION_CHANGED:
                            break;
                        case PeerManagerEvent.ET_PIECE_DEACTIVATED:
                            break;
                    }
                }
            });

        }

        @Override
        public void peerManagerRemoved(Download download, PeerManager peerManager) {
        }
    }
}
