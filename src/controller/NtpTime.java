package controller;

import java.io.IOException;
import java.net.*;

public class NtpTime {

    private String timeServer;
    private double offset = 0;
    private long updateTime;
    private long expireTime;

    private static NtpTime instance;

    public static NtpTime getInstance(String server) {
        if (instance == null) {
            NtpTime.instance = new NtpTime(server);
        }
        return instance;
    }

    public static NtpTime getInstance() {
        if (instance == null) {
            return null;
        }
        return instance;
    }

    private NtpTime(String server) {
        this.timeServer = server;
        this.expireTime = Long.MAX_VALUE;
        refresh();
    }

    public static void setExpireTime(long time) {
        NtpTime.getInstance().expireTime = time;
    }

    public static long getExpireTime() {
        return NtpTime.getInstance().expireTime;
    }

    public String getTimeServer() {
        return timeServer;
    }

    public double getOffset() {
        return offset;
    }

    public void refresh() {
        // Send message
        DatagramSocket socket;
        try {
            System.out.println("Refresching NtpTime...");
            socket = new DatagramSocket();
            InetAddress address = InetAddress.getByName(timeServer);
            byte[] buf = new NtpMessage().toByteArray();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 123);
            NtpMessage.encodeTimestamp(packet.getData(), 40, (System.currentTimeMillis() / 1000.0) + 2208988800.0);
            socket.send(packet);

            // Get response
            socket.receive(packet);

            // Immediately record the incoming timestamp
            double destinationTimestamp = (System.currentTimeMillis() / 1000.0) + 2208988800.0;

            NtpMessage msg = new NtpMessage(packet.getData());
            // Corrected, according to RFC2030 errata
            // double roundTripDelay = (destinationTimestamp -
            // msg.originateTimestamp)
            // - (msg.transmitTimestamp - msg.receiveTimestamp);

            double localClockOffset = ((msg.receiveTimestamp - msg.originateTimestamp)
                    + (msg.transmitTimestamp - destinationTimestamp)) / 2;

            offset = localClockOffset;
            updateTime = System.currentTimeMillis();

            socket.close();
            System.out.println("NtpTime set correctly");
        } catch (SocketException e) {
            e.printStackTrace();
            System.out.println("NtpTime error!");
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.out.println("NtpTime error!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("NtpTime error!");
        }
    }

    public long now() {
        if (expireTime > 0 && System.currentTimeMillis() - updateTime > expireTime) {
            System.out.println("Update Time " + (System.currentTimeMillis() - updateTime) + " " + expireTime);
            refresh();
        }
        return System.currentTimeMillis() + (long) offset;
    }

    private class TimeServerNotSetException extends Throwable {
    }
}
