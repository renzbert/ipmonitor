// original code from
// http://stackoverflow.com/questions/15968883/how-to-zip-a-folder-itself-using-java
package controller.evidence;

import controller.Utility;
import model.ObservedPeer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class provides methods to pack and sign the collected evidence.
 */
public class Packer {

    /**
     * This method packs the directory at the given path into a zip file.
     *
     * @param path         output directory
     * @param observedPeer
     * @return the path of the created zip file
     */
    public static String pack(String path, ObservedPeer observedPeer) {
        byte[] buffer = new byte[1024];
        String sourceFolder = path;
        String source = "";
        String zipFile = "";
        String peerId = Utility.byteArray2Hex(observedPeer.getId());
        String peerIp = observedPeer.getIp();
        if (peerId != null && peerId.length() > 0) {
            zipFile = path + File.separator + Utility.byteArray2Hex(observedPeer.getId()) + ".zip";
        } else {
            zipFile = path + File.separator + peerIp + ".zip";
        }
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        List<String> fileList = new ArrayList<String>();
        generateFileList(fileList, new File(path), sourceFolder);

        try {
            try {
                source = sourceFolder.substring(sourceFolder.lastIndexOf("\\") + 1, sourceFolder.length());
            } catch (Exception e) {
                source = sourceFolder;
            }
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output " + source + "to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file : fileList) {
                System.out.println("File Added : " + file);
                // ZipEntry ze = new ZipEntry(source + File.separator + file);
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(sourceFolder + File.separator + file);
                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return zipFile;
    }

    private static void generateFileList(List<String> fileList, File node, String sourceFolder) {
        if (node.isFile()) {
            String file = node.toString();
            fileList.add(file.substring(sourceFolder.length() + 1, file.length()));
        }
        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(fileList, new File(node, filename), sourceFolder);
            }
        }
    }
}
