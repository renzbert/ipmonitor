package controller.evidence;
//Code from https://docs.oracle.com/javase/tutorial/security/apisign/examples/VerSig.java

import java.io.*;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Class to provide signing functionality.
 */
public class Security {
    /**
     * This method generates a RSA signature for the generated zip file.
     * original code from
     * https://docs.oracle.com/javase/tutorial/security/apisign/gensig.html
     *
     * @param path           path to the file
     * @param file           the output file
     * @param privateKeyFile the private key
     */
    public static void genSig(String path, String file, String privateKeyFile) {
        try {
            String name = (new File(file)).getName();
            if (name.toLowerCase().endsWith(".zip")) {
                name = name.substring(0, name.length() - 4);
            }

            byte[] keyBytes = Files.readAllBytes(new File(privateKeyFile).toPath());
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey privKey = kf.generatePrivate(spec);
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initSign(privKey);

            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bufin = new BufferedInputStream(fis);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = bufin.read(buffer)) >= 0) {
                sig.update(buffer, 0, len);
            }
            bufin.close();
            byte[] realSig = sig.sign();

			/* save the signature in a file */
            FileOutputStream sigfos = new FileOutputStream(path + File.separator + name + ".sig");
            sigfos.write(realSig);
            sigfos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method verifies a signature with the given key, signature and the file.
     * Return an integer containing the results of the verification.
     * <p>
     * 1: verifies OK
     * -1: Problems with file reading
     * -100: problems with signature
     *
     * @param keyFile  file containing the public key
     * @param sigFile  file containing the signature
     * @param dataFile the file to verify
     * @return value of the result of the verification
     */
    public static int verSig(String keyFile, String sigFile, String dataFile) {
        int result = -100;
        try {

			/* import encoded public key */

            FileInputStream keyfis = new FileInputStream(keyFile);
            byte[] encKey = new byte[keyfis.available()];
            keyfis.read(encKey);

            keyfis.close();

            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);

			/* input the signature bytes */
            FileInputStream sigfis = new FileInputStream(sigFile);
            byte[] sigToVerify = new byte[sigfis.available()];
            sigfis.read(sigToVerify);

            sigfis.close();

			/* create a Signature object and initialize it with the public key */
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(pubKey);

			/* Update and verify the data */

            FileInputStream datafis = new FileInputStream(dataFile);
            BufferedInputStream bufin = new BufferedInputStream(datafis);

            byte[] buffer = new byte[1024];
            int len;
            while (bufin.available() != 0) {
                len = bufin.read(buffer);
                sig.update(buffer, 0, len);
            }
            ;

            bufin.close();

            boolean verifies = sig.verify(sigToVerify);

            result = verifies ? 1 : 0;

        } catch (SignatureException e) {
            e.printStackTrace();
            result = -100;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            result = -1;
        } catch (IOException e) {
            e.printStackTrace();
            result = -1;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            result = -100;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            result = -100;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            result = -100;
        }
        return result;
    }

    /**
     * Method to generate RSA key pair to be used in this plugin.
     *
     * @param keylength
     * @param path
     * @return
     */
    public static boolean genKeyPair(int keylength, String path) {
        KeyPairGenerator keyGen = null;
        SecureRandom random = null;
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        keyGen.initialize(keylength, random);

        KeyPair pair = keyGen.generateKeyPair();
        PrivateKey priv = pair.getPrivate();
        PublicKey pub = pair.getPublic();

        try {
            byte[] privKey = priv.getEncoded();
            FileOutputStream keyfos = keyfos = new FileOutputStream(path + File.separator + "privateKey");

            keyfos.write(privKey);
            keyfos.flush();
            keyfos.close();

            byte[] pubKey = pub.getEncoded();
            keyfos = new FileOutputStream(path + File.separator + "publicKey");

            keyfos.write(pubKey);
            keyfos.flush();
            keyfos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
