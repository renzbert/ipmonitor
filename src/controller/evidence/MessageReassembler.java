package controller.evidence;

import controller.Utility;
import model.InterceptedMessage;
import model.ReassembledFile;
import model.ReassembledPiece;
import org.gudy.azureus2.plugins.messaging.bittorrent.BTMessagePiece;
import org.gudy.azureus2.plugins.torrent.Torrent;
import org.gudy.azureus2.plugins.torrent.TorrentFile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides methods to reassemble pieces and files.
 */
public class MessageReassembler {

    /**
     * This method reassembles BT_PIECE messages to complete pieces.
     *
     * @param inList  ingoing message list
     * @param torrent torrent file for meta data
     * @param isAz    flag if the peer uses the Azureus messaging protocol
     * @return a map of the reassembled pieces
     */
    public static Map<Long, ReassembledPiece> ReassembleMessages(final List<InterceptedMessage> inList, final Torrent torrent,
                                                                 boolean isAz) {

        long pieceSize = torrent.getPieceSize();
        long lastPieceSize = torrent.getSize() - pieceSize * torrent.getPieceCount() + pieceSize;
        Map<Long, ReassembledPiece> pieceMap = new HashMap<Long, ReassembledPiece>();
        Map<Long, ReassembledPiece> pieceList = new HashMap<Long, ReassembledPiece>();
        Tuple2<Long, Long> metaData;
        long key;
        long offset;
        int metaLength = 9;
        ReassembledPiece piece;
        for (InterceptedMessage m : inList) {
            if (m.getMessage().getID().equals("BT_PIECE")) {
                metaData = extractKey(m.getMessage().getPayload());
                key = metaData.getF1();
                offset = metaData.getF2();
                BTMessagePiece btPiece = (BTMessagePiece) m.getMessage();
                if (offset < 0 || key < 0)
                    break;
                if (!pieceMap.containsKey(key)) {
                    if (key == torrent.getPieceCount() - 1) {
                        piece = new ReassembledPiece(key, lastPieceSize, m.getTime());
                    } else {
                        piece = new ReassembledPiece(key, pieceSize, m.getTime());
                    }
                    pieceMap.put(key, piece);
                } else {
                    piece = pieceMap.get(key);
                }

                if (isAz) {
                    metaLength = getMetaLength(m.getData());
                }
                piece.addData(m.getTime(), offset, Arrays.copyOfRange(m.getData(), metaLength, m.getData().length));
            }
        }

        for (ReassembledPiece p : pieceMap.values()) {
            try {
                if (p.isComplete()) {
                    byte[] hash = Utility.SHAsum(p.getData());
                    byte[] original = torrent.getPieces()[(int) p.getIndex()];

                    boolean equal = true;
                    for (int i = 0; i < hash.length; i++) {
                        if (hash[i] != original[i]) {
                            equal = false;
                            break;
                        }
                    }
                    if (equal) {
                        pieceList.put(p.getIndex(), p);
                    }
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return pieceList;
    }

    private static int getMetaLength(byte[] data) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.rewind();

        int messageTypeSize = byteBuffer.getInt();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < messageTypeSize; i++) {
            sb.append(byteBuffer.get());
        }
        String messageType = sb.toString();
        byte version = byteBuffer.get();
        boolean padding = ((version >> 4) & 1) == 1;
        if (padding) {
            short paddingLength = byteBuffer.getShort();
            for (int i = 0; i < paddingLength; i++) {
                byteBuffer.get();
            }
        }
        byteBuffer.getInt();
        byteBuffer.getInt();
        return byteBuffer.position();
    }

    /**
     * This method reassembles recovered pieces to files.
     * Also incomplete files will re reassembled with null bytes, when there is no information.
     * Files without any pieces will be skiped.
     *
     * @param pieces  map of reassembled pieces
     * @param torrent torrent file for meta data
     * @param path    path to output directory
     * @return a map of reassembled files
     */
    public static Map<String, ReassembledFile> ReassamblePieces(final Map<Long, ReassembledPiece> pieces, final Torrent torrent,
                                                                final String path) {

        HashMap<String, ReassembledFile> fileMap = new HashMap<String, ReassembledFile>();
        TorrentFile[] fileList = torrent.getFiles();
        ReassembledFile file;
        ReassembledPiece piece;
        long byteStartRange = 0;
        long byteEndRange = 0;
        long pieceSize = torrent.getPieceSize();
        long startPiece;
        long endPiece;
        long fileStartPointer = 0;
        long fileEndPointer = 0;
        for (TorrentFile tf : fileList) {
            fileStartPointer = fileEndPointer;
            fileEndPointer = fileEndPointer + tf.getSize();
            byteEndRange = byteStartRange + tf.getSize();
            startPiece = (long) Math.floor((double) byteStartRange / (double) pieceSize);
            endPiece = (long) Math.floor((double) byteEndRange / (double) pieceSize);

            file = new ReassembledFile(tf.getName(), tf.getSize(), endPiece - startPiece + 1, path);
            fileMap.put(file.getName(), file);
            long pieceOffset;
            long length;
            long fileOffset = 0;
            boolean complete = true;
            for (long i = startPiece; i <= endPiece; i++) {
                piece = pieces.get(i);
                if (piece != null) {
                    // Write pieceData to file
                    pieceOffset = (byteStartRange > i * pieceSize) ? (byteStartRange % pieceSize) : 0;
                    length = (byteEndRange < (i + 1) * pieceSize) ? (byteEndRange % pieceSize)
                            : pieceSize - pieceOffset;

                    try {

                        file.addData(fileOffset, length,
                                Arrays.copyOfRange(piece.getData(), (int) pieceOffset, (int) (pieceOffset + length)),
                                piece.getStartTime(), piece.getEndTime());
                    } catch (IOException e) {
                        e.printStackTrace();
                        file.setComplete(false);
                        break;
                    }
                    fileOffset += length;
                    if (i == endPiece) {// Complete file
                        complete = true;
                    }
                } else {// File Incomplete
                    complete = false;
                    if (i == startPiece && fileStartPointer != startPiece * pieceSize) {
                        fileOffset = startPiece * pieceSize - fileStartPointer;
                    } else {
                        fileOffset += pieceSize;
                    }
                }
            }
            file.setComplete(file.getPieceCnt() == file.getPieces());
            byteStartRange = byteEndRange;
        }

        return fileMap;
    }

    private static Tuple2<Long, Long> extractKey(ByteBuffer[] payload) {
        long index;
        long offset;

        try {
            ByteBuffer b = payload[0];
            b.rewind();

            index = b.getInt();
            offset = b.getInt();

        } catch (NullPointerException e) {
            index = -1;
            offset = -1;
        }
        return new Tuple2<Long, Long>(index, offset);
    }

    /**
     * Helper Method to get a human readable string of a {@link ByteBuffer}s in hex format.
     *
     * @param byteBuffers
     * @return String representation in hex format
     */
    public static String printByteBuffer(ByteBuffer[] byteBuffers) {
        StringBuffer sb = new StringBuffer("StringBuffer:\n");
        ByteBuffer b;
        for (int i = 0; i < byteBuffers.length; i++) {
            b = byteBuffers[i].duplicate();
            b.rewind();
            while (b.hasRemaining()) {
                byte b1 = b.get();
                sb.append(String.format("%02X", b1));
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public static class Tuple2<T1, T2> {
        private T1 f1;
        private T2 f2;

        public Tuple2(T1 f1, T2 f2) {
            this.f1 = f1;
            this.f2 = f2;
        }

        public T1 getF1() {
            return f1;
        }

        public T2 getF2() {
            return f2;
        }
    }

}
