package controller.evidence;

import model.InterceptedMessage;
import model.ObservedPeer;
import model.ReassembledPiece;
import org.gudy.azureus2.core3.util.BDecoder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Class to provide a method to create the history.xml file.
 */
public class HistoryExtractor {

    /**
     * Extract the history information out of the in message list and writes it to an XML file.
     *
     * @param inList the ingoing message list
     * @param pieces map with reassembled pieces and their index as key
     * @param peer   the observedPeer
     * @param path   the path to store the created file
     */
    public static void extractHistory(final List<InterceptedMessage> inList, Map<Long, ReassembledPiece> pieces, ObservedPeer peer,
                                      String path) {
        byte[] bitfield = new byte[(int) peer.getTorrent().getPieceCount()];
        for (byte b : bitfield) {
            b = 0;
        }

        List<InterceptedMessage> messageList = new ArrayList<InterceptedMessage>();
        for (InterceptedMessage m : inList) {
            switch (m.getMessage().getID()) {
                case "BT_BITFIELD":
                case "BT_HAVE_ALL":
                case "BT_HAVE_NONE":
                case "BT_HAVE":
                case "AZ_HAVE":
                    messageList.add(m);
                    break;
            }
        }
        List<Object> list = new ArrayList<Object>(pieces.values());
        list.addAll(messageList);

        Collections.sort(list, new Comparator<Object>() {
            @Override
            public int compare(Object a, Object b) {
                long t1 = getTime(a);
                long t2 = getTime(b);
                return (int) (t1 - t2);
            }

            private long getTime(Object x) {
                if (x instanceof InterceptedMessage) {
                    return ((InterceptedMessage) x).getTime();
                } else if (x instanceof ReassembledPiece) {
                    return ((ReassembledPiece) x).getStartTime();
                } else {
                    return 0;
                }
            }
        });

        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();

            final Element shareHistoryElement = document.createElement("sharehistory");
            document.appendChild(shareHistoryElement);

            InfoExtractor.createHeaderElement(peer, document, shareHistoryElement);

            // History
            final Element historyElement = document.createElement("history");
            shareHistoryElement.appendChild(historyElement);
            for (Object m : list) {
                final Element entryElement = document.createElement("entry");
                historyElement.appendChild(entryElement);
                if (m instanceof InterceptedMessage) {
                    InterceptedMessage msg = (InterceptedMessage) m;
                    // final Element timeElement =
                    // document.createElement("date");
                    entryElement.setAttribute("date", new Date(((InterceptedMessage) m).getTime()).toString());
                    // entryElement.appendChild(timeElement);
                    final Element messageElement = document.createElement("message");
                    messageElement.setAttribute("type", msg.getMessage().getID());
                    entryElement.appendChild(messageElement);
                    switch (msg.getMessage().getID()) {
                        case "BT_BITFIELD":
                            messageElement.setTextContent("Bitfield update");
                            int offset = peer.isAz() ? 16 : 1;
                            updateBitfield(bitfield, msg.getData(), offset);
                            break;
                        case "BT_HAVE":
                            final Element pieceElement = document.createElement("piece");
                            ByteBuffer wrapped = ByteBuffer.wrap(msg.getData());
                            int index = wrapped.getInt();
                            pieceElement.setAttribute("index", index + "");
                            updateBitfield(bitfield, index);
                            messageElement.appendChild(pieceElement);
                            break;
                        case "BT_HAVE_ALL":
                            Arrays.fill(bitfield, (byte) 1);
                            break;
                        case "BT_HAVE_NONE":
                            Arrays.fill(bitfield, (byte) 0);
                            break;
                        case "AZ_HAVE":
                            if (msg.getMessage() != null) {
                                try {
                                    Map<String, Object> map = BDecoder.decode(msg.getData());
                                    for (Object o : map.values()) {
                                        Integer i = (Integer) o;
                                        final Element pieceAZElement = document.createElement("piece");
                                        pieceAZElement.setAttribute("index", i + "");
                                        messageElement.appendChild(pieceAZElement);
                                        updateBitfield(bitfield, i);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (ClassCastException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                    }
                } else if (m instanceof ReassembledPiece) {
                    ReassembledPiece piece = (ReassembledPiece) m;
                    bitfield[(int) piece.getIndex()] = 1;
                    // final Element timeElement =
                    // document.createElement("date");
                    entryElement.setAttribute("date", new Date(piece.getStartTime()).toString());
                    // entryElement.appendChild(timeElement);
                    final Element pieceElement = document.createElement("piece_downloaded");
                    pieceElement.setAttribute("index", piece.getIndex() + "");
                    entryElement.appendChild(pieceElement);
                }
                final Element bitfieldElement = document.createElement("bitfield");
                bitfieldElement.setTextContent(printBitfield(bitfield));
                entryElement.appendChild(bitfieldElement);
                final Element percentElement = document.createElement("percent");
                percentElement.setTextContent(getPercent(bitfield));
                entryElement.appendChild(percentElement);
            }

            // Output
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            // transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
            // LIBRARY_DTD);
            transformer.transform(new DOMSource(shareHistoryElement), new StreamResult(new FileOutputStream(path)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static void updateBitfield(byte[] bitfield, byte[] data, int offset) {
        try {
            byte[] d = Arrays.copyOfRange(data, offset, data.length);
            int index = 0;
            for (int j = 0; j < d.length; j++) {
                byte b = d[j];
                for (int i = 0; i < 8; i++) {
                    if ((b >> (7 - i) & 1) == 1) {
                        index = 8 * j + i;
                        if (index < bitfield.length)
                            bitfield[8 * j + i] = 1;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateBitfield(byte[] bitfield, int index) {

    }

    private static String printBitfield(byte[] bitfield) {
        StringBuffer sb = new StringBuffer();
        for (byte b : bitfield) {
            sb.append(b);
        }
        return sb.toString();
    }

    private static String getPercent(byte[] bitfield) {
        int cnt = 0;
        for (byte b : bitfield) {
            if (b == 1)
                cnt++;
        }
        double percent = ((double) cnt / (double) bitfield.length) * (double) 100;
        return percent + "%";
    }

    /**
     * Comparator for comparing timestamps of the intercepted messages.
     */
    public class TimeComperator implements Comparator<Object> {

        @Override
        public int compare(Object a, Object b) {
            long t1 = getTime(a);
            long t2 = getTime(b);
            return (int) (t1 - t2);
        }

        private long getTime(Object x) {
            if (x instanceof InterceptedMessage) {
                return ((InterceptedMessage) x).getTime();
            } else if (x instanceof ReassembledPiece) {
                return ((ReassembledPiece) x).getStartTime();
            } else {
                return 0;
            }
        }

    }

}
