package controller.evidence;

import controller.NtpTime;
import controller.Utility;
import model.ReassembledPiece;
import model.ReassembledFile;
import model.ObservedPeer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

/**
 * Class to provide a method to create the info.xml file.
 */
public class InfoExtractor {

    /**
     * Extract information of the observed peer and writes it to an XML file.
     *
     * @param peer      the observed peer
     * @param pieceList list of downloaded pieces from the peer
     * @param fileList  the files of the torrent
     * @param path      the path to store the created file
     */
    public static void extractInfo(ObservedPeer peer, Map<Long, ReassembledPiece> pieceList, Map<String, ReassembledFile> fileList, String path) {

        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();

            final Element infoElement = document.createElement("info");
            document.appendChild(infoElement);

            createHeaderElement(peer, document, infoElement);

            //FileList
            final Element fileListElement = createAndAddElement("file-list", infoElement, document);
            for (ReassembledFile file : fileList.values()) {
                final Element fileElement = createAndAddElement("file", fileListElement, document);
                fileElement.setAttribute("name", file.getName());
                fileElement.setAttribute("size", file.getSize() + " bytes");
                fileElement.setAttribute("complete", file.isComplete() + "");
                final Element progressElement = createAndAddElement("progress", fileElement, document);
                progressElement.setAttribute("piece-count", file.getPieceCnt() + "");
                progressElement.setAttribute("total-piece-count", file.getPieces() + "");
                if (file.getPieceCnt() > 0) {
                    final Element startElement = createAndAddElement("start-time", fileElement, document);
                    startElement.setTextContent((new Date(file.getStartTime()).toString()));
                    final Element endElement = createAndAddElement("end-time", fileElement, document);
                    endElement.setTextContent((new Date(file.getEndTime()).toString()));
                }
            }

            //TimeServer
            final Element timeElement = createAndAddElement("time-server", infoElement, document);
            NtpTime ntpTime = NtpTime.getInstance();
            timeElement.setAttribute("time-server", ntpTime.getTimeServer());
            timeElement.setAttribute("offset", ntpTime.getOffset() + "");

            //TorExitNodeAddress
            final Element torElement = createAndAddElement("tor-exit-node-list", infoElement, document);
            torElement.setAttribute("url", "https://check.torproject.org/exit-addresses");

            // Output
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            // transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
            // LIBRARY_DTD);
            transformer.transform(new DOMSource(infoElement), new StreamResult(new FileOutputStream(path)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void createHeaderElement(ObservedPeer peer, Document document, Element parentElement) {
        final Element headerElement = document.createElement("header");
        parentElement.appendChild(headerElement);

        final Element peerElement = document.createElement("peer");
        headerElement.appendChild(peerElement);

        final Element ipElement = document.createElement("ip");
        ipElement.setTextContent(peer.getIp());
        peerElement.appendChild(ipElement);

        final Element idElement = document.createElement("id");
        idElement.setTextContent(Utility.byteArray2Hex(peer.getId()));
        peerElement.appendChild(idElement);

        final Element dateElement = document.createElement("date");
        dateElement.setTextContent(peer.getDate().toString());
        peerElement.appendChild(dateElement);

        final Element clientElement = document.createElement("client");
        clientElement.setTextContent(peer.getClient());
        peerElement.appendChild(clientElement);

        final Element torrentElement = document.createElement("torrent");
        headerElement.appendChild(torrentElement);

        final Element torrentNameElement = document.createElement("name");
        torrentNameElement.setTextContent(peer.getTorrent().getName());
        torrentElement.appendChild(torrentNameElement);

        final Element hashElement = document.createElement("infohash");
        hashElement.setTextContent(Base64.getEncoder().encodeToString(peer.getTorrent().getHash()));
        torrentElement.appendChild(hashElement);
    }

    private static Element createAndAddElement(String name, Element parent, Document document) {
        final Element element = document.createElement(name);
        parent.appendChild(element);
        return element;
    }

    private static Element createAndAddElementWithText(String name, String text, Element parent, Document document) {
        Element element = createAndAddElement(name, parent, document);
        element.setTextContent(text);
        return element;
    }
}
