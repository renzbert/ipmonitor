package controller.evidence;

import model.InterceptedMessage;
import model.ObservedPeer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class to provide a method to create the message.xml file.
 */
public class MessageExtractor {
    /**
     * Extract the messages of the observed peer and writes it to an XML file. It will create a message summary
     * as well as separate list of ingoing and outgoing messages.
     *
     * @param peer the observed peer
     * @param path the path to store the created file
     */
    public static void extractMessages(ObservedPeer peer, String path) {

        List<InterceptedMessage> inList = peer.getInMsgList().stream().sorted(Comparator.comparingLong(InterceptedMessage::getTime)).collect(Collectors.toList());
        List<InterceptedMessage> outList = peer.getOutMsgList().stream().sorted(Comparator.comparingLong(InterceptedMessage::getTime)).collect(Collectors.toList());

        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();

            //RootElement
            final Element messagesElement = document.createElement("messages");
            document.appendChild(messagesElement);

            InfoExtractor.createHeaderElement(peer, document, messagesElement);

            //MessageSummary
            final Element messageSummaryElement = createAndAddElement("messages-summary", messagesElement, document);

            final Element totalMessagesAmountElement = createAndAddElement("total-message-count", messageSummaryElement, document);
            totalMessagesAmountElement.setTextContent((inList.size() + outList.size()) + "");

            final Element inListSummary = createAndAddElement("in-message-list-summary", messageSummaryElement, document);
            if (inList.isEmpty()) {
                inListSummary.setTextContent("No in-messages available.");
            } else {
                addMessageListInfo(inList, inListSummary, document);
            }
            final Element outListSummary = createAndAddElement("out-message-list-summary", messageSummaryElement, document);
            if (outList.isEmpty()) {
                outListSummary.setTextContent("No out-messages available.");
            } else {
                addMessageListInfo(outList, outListSummary, document);
            }

            //Messages
            final Element inMessageListElement = document.createElement("in-message-list");
            messagesElement.appendChild(inMessageListElement);
            if (inList.isEmpty()) {
                inMessageListElement.setTextContent("No in-messages available.");
            } else {
                for (InterceptedMessage message : inList) {
                    addMessageElement(message, inMessageListElement, document);
                }
            }

            final Element outMessageListElement = document.createElement("out-message-list");
            messagesElement.appendChild(outMessageListElement);
            if (outList.isEmpty()) {
                inMessageListElement.setTextContent("No out-messages available.");
            } else {
                for (InterceptedMessage message : outList) {
                    addMessageElement(message, outMessageListElement, document);
                }
            }

            // Output
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            // transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
            // LIBRARY_DTD);
            transformer.transform(new DOMSource(messagesElement), new StreamResult(new FileOutputStream(path)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    private static Element createAndAddElement(String name, Element parent, Document document) {
        final Element element = document.createElement(name);
        parent.appendChild(element);
        return element;
    }

    private static void addMessageListInfo(List<InterceptedMessage> list, Element messageSummaryElement, Document document) {
        final Element sizeElement = createAndAddElement("size", messageSummaryElement, document);
        sizeElement.setTextContent(list.size() + "");

        Map<String, Integer> map = new HashMap<>();
        for (InterceptedMessage message : list) {
            String id = message.getMessage().getID();
            if (map.containsKey(id)) {
                map.put(id, map.get(id) + 1);
            } else {
                map.putIfAbsent(id, 1);
            }
        }

        for (String key : map.keySet()) {
            final Element messageTypeElement = createAndAddElement("message-type", messageSummaryElement, document);
            messageTypeElement.setAttribute("id", key);
            messageTypeElement.setAttribute("count", map.get(key) + "");
        }
    }

    private static void addMessageElement(InterceptedMessage message, Element messageListElement, Document document) {
        final Element messageElement = document.createElement("message");
        messageElement.setAttribute("id", message.getMessage().getID());

        //Date
        final Element dateElement = createAndAddElement("date", messageElement, document);
        dateElement.setTextContent((new Date(message.getTime())).toString());

        //Time
        final Element timeElement = createAndAddElement("time", messageElement, document);
        timeElement.setTextContent(message.getTime() + "");

        //Description
        final Element descriptionElement = document.createElement("description");
        descriptionElement.setTextContent(message.getMessage().getDescription());
        messageElement.appendChild(descriptionElement);

        //Payload
        if (message.getData() != null) {
            final Element payloadElement = document.createElement("payload");
            payloadElement.setTextContent(Base64.getEncoder().encodeToString(message.getData()));
            messageElement.appendChild(payloadElement);

        }

        messageListElement.appendChild(messageElement);
    }
}
