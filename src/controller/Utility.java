package controller;

import org.gudy.azureus2.countrylocator.CountryLocator;
import org.gudy.azureus2.plugins.peers.Peer;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * Utility class for helper methods.
 */
public class Utility {

    static CountryLocator countryLocator = new CountryLocator();

    public static CountryLocator getCountryLocator() {
        return countryLocator;
    }

    /**
     * Mapping state value to string representation.
     *
     * @param state
     * @return
     */
    public static String stateToString(int state) {
        switch (state) {
            case Peer.CLOSING:
                return "CLOSING";
            case Peer.CONNECTING:
                return "CONNECTING";
            case Peer.DISCONNECTED:
                return "DISCONNECTED";
            case Peer.HANDSHAKING:
                return "HANDSHAKING";
            case Peer.TRANSFERING:
                return "TRANSFERING";
        }
        return "UNKNOWN STATE";
    }

    /**
     * Calculates SHA-1 hash of given ByteArray.
     *
     * @param data to be hashed
     * @return ByteArray with SHA-1 hash
     * @throws NoSuchAlgorithmException
     */
    public static byte[] SHAsum(byte[] data) throws NoSuchAlgorithmException {
        // from
        // http://stackoverflow.com/questions/1515489/compute-sha-1-of-byte-array
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        /*FileWriter fw;
        try {
			fw = new FileWriter("out.txt");
			fw.write(byteArray2Hex(data));
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
        return md.digest(data);
    }

    public static String byteArray2Hex(final byte[] data) {
        // from
        // http://stackoverflow.com/questions/1515489/compute-sha-1-of-byte-array
        Formatter formatter = new Formatter();
        for (byte b : data) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
