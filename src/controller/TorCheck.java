package controller;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to provide ip addresses of tor exit nodes. The adresses will be fetch from
 * the official servers of the TOR project @https://check.torproject.org/exit-addresses.
 * <p>
 * To keep the list up do date it is possible to specify a expire time when an update is triggered.
 */
public class TorCheck {
    /**
     * List of tor exit nodes.
     */
    private List<String> torExitNodes;
    /**
     * Timestamp of last update of the torExitNodes.
     */
    private long updateTime;
    /**
     * Interval when to update the torExitNodes again.
     */
    private long expireTime;

    private TorCheck() {
        torExitNodes = new ArrayList<String>();
        updateTime = System.currentTimeMillis();
        expireTime = 5000; //default
    }

    private static TorCheck instance;

    public static TorCheck getInstance() {
        if (instance == null) {
            TorCheck.instance = new TorCheck();
        }
        return instance;
    }

    public static void main(String[] args) {
        while (true) {
            TorCheck.getInstance().getTorExitList();
        }
    }

    public static void setExpireTime(long time) {
        TorCheck.getInstance().expireTime = time;
    }

    public static long getExpireTime() {
        return TorCheck.getInstance().expireTime;
    }

    /**
     * Fetches the newest tor exit node list from the official tor servers.
     * URL: https://check.torproject.org/exit-addresses
     * <p>
     * If the last update is within the expireTime, the cached list will be immediately returned.
     *
     * @return a list of tor exit node ip addresses
     */
    public List<String> getTorExitList() {

        long time = System.currentTimeMillis();

        if (torExitNodes.size() > 0 && time - updateTime < expireTime) {
            return torExitNodes;
        } else {
            System.out.println("Refreshing TOR-exit-list...");
            String https_url = "https://check.torproject.org/exit-addresses";
            URL url;
            try {
                url = new URL(https_url);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                extractIps(connection);
                updateTime = System.currentTimeMillis();
                System.out.println("TOR-exit-list update complete");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                System.out.println("TOR-exit-list address-error");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("TOR-exit-list connection-error");
            }
            return torExitNodes;
        }
    }

    private void extractIps(HttpsURLConnection connection) {
        if (connection != null) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String input;
                torExitNodes = new ArrayList<>();
                while ((input = bufferedReader.readLine()) != null) {
                    if (input.startsWith("ExitAddress")) {
                        torExitNodes.add(input.split(" ")[1]);
                    }
                }
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
