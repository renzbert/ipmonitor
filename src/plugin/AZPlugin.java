package plugin;

import org.eclipse.swt.widgets.Composite;
import org.gudy.azureus2.plugins.Plugin;
import org.gudy.azureus2.plugins.PluginException;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.ui.UIInstance;
import org.gudy.azureus2.plugins.ui.UIManagerListener;
import org.gudy.azureus2.ui.swt.plugins.UISWTInstance;
import org.gudy.azureus2.ui.swt.plugins.UISWTViewEvent;
import org.gudy.azureus2.ui.swt.plugins.UISWTViewEventListener;
import view.MainView;

import java.util.HashMap;

public class AZPlugin implements Plugin {

    private UISWTInstance swtInstance = null;
    private boolean bLaunchOnStart = true;
    private final String VIEWID = "ipmonitor";
    private PluginInterface plugin_interface;
    private HashMap<Integer, String> params = new HashMap<Integer, String>();

    @Override
    public void initialize(PluginInterface pi) throws PluginException {
        plugin_interface = pi;
        initConfig();
        initMainView();
    }

    private void initConfig() {


        plugin_interface.getDownloadManager().stopAllDownloads();

    }

    private void initMainView() {
        plugin_interface.getUIManager().addUIListener(new UIManagerListener() {
            private ViewListener view_listener;

            public void UIAttached(UIInstance instance) {
                if (instance instanceof UISWTInstance) {

                    swtInstance = (UISWTInstance) instance;
                    view_listener = new ViewListener();

                    swtInstance.addView(UISWTInstance.VIEW_MAIN, VIEWID,
                            view_listener);

                    if (bLaunchOnStart)
                        swtInstance.openMainView(VIEWID, view_listener, null);
                }
            }

            public void UIDetached(UIInstance instance) {

            }
        });
    }

    private class ViewListener implements UISWTViewEventListener {

        private MainView view;
        private boolean isCreated;

        @Override
        public boolean eventOccurred(UISWTViewEvent event) {
            switch (event.getType()) {
                case UISWTViewEvent.TYPE_CREATE:
                    if (isCreated)
                        return false;

                    isCreated = true;
                    break;

                case UISWTViewEvent.TYPE_INITIALIZE: {

                    Composite comp = (Composite) event.getData();

                    view = new MainView(plugin_interface, params);

                    view.initialize(comp);

                    break;
                }
                case UISWTViewEvent.TYPE_DESTROY: {
                    if (view != null) {

                        view.delete();

                        view = null;
                    }

                    isCreated = false;

                    break;
                }
            }

            return true;
        }

    }

}
