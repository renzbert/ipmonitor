package view;

import controller.IpMonitor;
import controller.NtpTime;
import controller.TorCheck;
import controller.evidence.Security;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.gudy.azureus2.plugins.PluginConfig;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.ui.config.*;
import org.gudy.azureus2.plugins.ui.model.BasicPluginConfigModel;
import view.evidence.EvidenceTab;

import java.util.HashMap;

/**
 * MainView of the UI
 *
 * Initializes config section of the plugin as well as the startup.
 */
public class MainView {

    PluginInterface plugin_interface;
    HashMap<Integer, String> params;
    IpMonitor ipm;

    TorrentTab toT;
    TargetTabFolder taT;
    EvidenceTab evT;

    Display display;
    Composite panel;
    TabFolder tabFolder;
    TabItem tabTorrents;

    BasicPluginConfigModel config_page;

    public MainView(PluginInterface plugin_interface, HashMap<Integer, String> params) {
        this.plugin_interface = plugin_interface;
        this.params = params;
    }

    public void initialize(final Composite composite) {
        Realm.runWithDefault(SWTObservables.getRealm(composite.getDisplay()), new Runnable() {
            public void run() {

                try {
                    ipm = new IpMonitor(plugin_interface);
                    initUI(composite);

                    String timeserverURL = plugin_interface.getPluginconfig().getPluginStringParameter("timeserverURL");
                    if(timeserverURL==null|| timeserverURL.length()<=0) {
                        timeserverURL= plugin_interface.getPluginProperties().getProperty("plugin.time.defaultserver");
                    }
                    ipm.initTime(timeserverURL);


                    if (plugin_interface.getPluginconfig().getPluginBooleanParameter("refreshTimeEnable")) {
                        NtpTime.setExpireTime(
                                plugin_interface.getPluginconfig().getPluginIntParameter("refreshInterval") * 1000);
                    } else {
                        NtpTime.setExpireTime(0);
                    }
                    if (plugin_interface.getPluginconfig().getPluginBooleanParameter("enableTor")) {
                        TorCheck.setExpireTime(
                                plugin_interface.getPluginconfig().getPluginIntParameter("TORrefreshInterval") * 1000);
                    } else {
                        TorCheck.setExpireTime(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void initUI(Composite composite) {
        creatConfig(plugin_interface, ipm);

        tabFolder = new TabFolder(composite, SWT.NONE);

        tabTorrents = new TabItem(tabFolder, SWT.NONE);
        toT = new TorrentTab(tabFolder, SWT.NONE, plugin_interface, ipm);
        tabTorrents.setText("Torrents");
        tabTorrents.setControl(toT);

        tabTorrents = new TabItem(tabFolder, SWT.NONE);
        taT = new TargetTabFolder(tabFolder, SWT.NONE, plugin_interface, ipm);
        tabTorrents.setText("Targets");
        tabTorrents.setControl(taT);

        tabTorrents = new TabItem(tabFolder, SWT.NONE);
        evT = new EvidenceTab(tabFolder, SWT.NONE, plugin_interface);
        tabTorrents.setText("Evidence");
        tabTorrents.setControl(evT);
    }

    private void creatConfig(PluginInterface plugin_interface2, IpMonitor ipm2) {
        PluginConfig pconfig = plugin_interface.getPluginconfig();
        // Create the configuration section.
        BasicPluginConfigModel config_page = plugin_interface.getUIManager().createBasicPluginConfigModel("ipmonitor");

        // Timeserver
        StringParameter timeserverURL = config_page.addStringParameter2("timeserverURL",
                "ipmonitor.config.time.timeserverurl", "at.pool.ntp.org");
        BooleanParameter refreshTimeEnable = config_page.addBooleanParameter2("refreshTimeEnable",
                "ipmonitor.config.time.refresh", false);
        IntParameter refreshInterval = config_page.addIntParameter2("refreshInterval", "ipmonitor.config.time.interval",
                600);
        config_page.createGroup("ipmonitor.config.time.title",
                new Parameter[]{timeserverURL, refreshTimeEnable, refreshInterval});
        refreshTimeEnable.addEnabledOnSelection(refreshInterval);

        // Default folders
        DirectoryParameter defaultTempFolder = config_page.addDirectoryParameter2("defaultTempFolder",
                "ipmonitor.config.folders.tmp", "");
        DirectoryParameter defaultFilterFolder = config_page.addDirectoryParameter2("defaultFilterFolder",
                "ipmonitor.config.folders.filter", "");
        DirectoryParameter defaultEvidenceFolder = config_page.addDirectoryParameter2("defaultEvidenceFolder",
                "ipmonitor.config.folders.evidence", "");
        config_page.createGroup("ipmonitor.config.folders.title",
                new Parameter[]{defaultTempFolder, defaultFilterFolder, defaultEvidenceFolder});

        FileParameter privateKeyFile = config_page.addFileParameter2("privateKeyFile", "ipmonitor.config.signature.privateKey",
                pconfig.getPluginStringParameter("defaultEvidenceFolder"), new String[]{"*"});
        FileParameter publicKeyFile = config_page.addFileParameter2("publicKeyFile", "ipmonitor.config.signature.publicKey",
                pconfig.getPluginStringParameter("defaultEvidenceFolder"), new String[]{"*"});
        ActionParameter createKeyFilesButton = config_page.addActionParameter2(
                "ipmonitor.config.signature.privateKey.button", "ipmonitor.config.signature.privateKey.button.label");
        createKeyFilesButton.addListener(new ParameterListener() {
            public void parameterChanged(Parameter p) {
                String path = pconfig.getPluginStringParameter("defaultEvidenceFolder");
                if (path == null) {
                    path = "";
                }
                Security.genKeyPair(2048, path);
            }
        });
        config_page.createGroup("ipmonitor.config.signature.title", new Parameter[]{privateKeyFile, publicKeyFile, createKeyFilesButton});

        // Filter
        FileParameter whitelistFilter = config_page.addFileParameter2("whitelistFilter",
                "ipmonitor.config.filter.white", pconfig.getPluginStringParameter("defaultFilterFolder"),
                new String[]{"*.wlf"});
        BooleanParameter whiteListDefaultEnabled = config_page.addBooleanParameter2("whiteListDefaultEnabled",
                "ipmonitor.config.filter.white.enable", false);
        FileParameter blacklistFilter = config_page.addFileParameter2("blacklistFilter",
                "ipmonitor.config.filter.black", pconfig.getPluginStringParameter("defaultFilterFolder"),
                new String[]{"*.blf"});
        BooleanParameter blackListDefaultEnabled = config_page.addBooleanParameter2("blackListDefaultEnabled",
                "ipmonitor.config.filter.black.enable", true);
        FileParameter countryFilter = config_page.addFileParameter2("countryFilter", "ipmonitor.config.filter.country",
                pconfig.getPluginStringParameter("defaultFilterFolder"), new String[]{"*.cf"});
        BooleanParameter countryDefaultEnabled = config_page.addBooleanParameter2("countryDefaultEnabled",
                "ipmonitor.config.filter.country.enable", true);
        BooleanParameter enableTor = config_page.addBooleanParameter2("enableTor", "ipmonitor.config.filter.tor", true);
        IntParameter TORrefreshInterval = config_page.addIntParameter2("TORrefreshInterval",
                "ipmonitor.config.filter.torinterval", 6000);
        enableTor.addEnabledOnSelection(TORrefreshInterval);
        config_page.createGroup("ipmonitor.config.filter.title",
                new Parameter[]{whitelistFilter, whiteListDefaultEnabled, blacklistFilter, blackListDefaultEnabled, countryFilter, countryDefaultEnabled, enableTor, TORrefreshInterval});
        config_page.addLabelParameter2("ipmonitor.config.restart");
    }

    public void delete() {
        toT.delete();
    }
}
