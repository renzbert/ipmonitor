package view;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlView extends Composite {

    /**
     * Create the composite.
     *
     * @param parent
     * @param style
     */

    File source;

    public XmlView(Composite parent, int style, File source) {
        super(parent, style);
        this.source = source;
        setLayout(new FillLayout(SWT.HORIZONTAL));

        if (source != null) {
            TreeViewer treeViewer = new TreeViewer(this, SWT.BORDER);
            treeViewer.setContentProvider(createTreeContentProvider());
            treeViewer.setLabelProvider(createLabelProvider());
            treeViewer.setInput(createLazyTreeModelFrom(source.getPath()));
            treeViewer.expandAll();
        }

    }


    //Code von https://www.tutorials.de/threads/xml-file-als-tree-in-swt-darstellen.262425/
    //author Thomas.Darimont
    private LabelProvider createLabelProvider() {
        return new LabelProvider() {
            public String getText(Object element) {
                Node node = (Node) element;
                if (null == node) {
                    return super.getText(element);
                } else if (Node.TEXT_NODE == node.getNodeType()) {
                    return node.getTextContent().trim();
                } else {
                    StringBuilder elementRepresentation = new StringBuilder("<");
                    elementRepresentation.append(node.getNodeName());
                    NamedNodeMap attributes = node.getAttributes();

                    if (null == attributes) {
                        elementRepresentation.append(">");
                        return elementRepresentation.toString();
                    }

                    int attributeCount = attributes.getLength();
                    for (int i = 0; i < attributeCount; i++) {
                        Node attributeNode = attributes.item(i);
                        elementRepresentation.append(" ");
                        elementRepresentation.append(attributeNode.getNodeName());
                        elementRepresentation.append("=\"");
                        elementRepresentation.append(attributeNode.getNodeValue());
                        elementRepresentation.append("\"");
                    }
                    elementRepresentation.append(">");
                    return elementRepresentation.toString();
                }
            }
        };
    }

    //Code von https://www.tutorials.de/threads/xml-file-als-tree-in-swt-darstellen.262425/
    //author Thomas.Darimont
    private IContentProvider createTreeContentProvider() {
        return new ITreeContentProvider() {
            public Object[] getChildren(Object parentElement) {
                NodeList nodeList = ((Node) parentElement).getChildNodes();
                int nodesCount = nodeList.getLength();
                List<Node> nodes = new ArrayList<Node>();
                for (int i = 0; i < nodesCount; i++) {
                    Node currentNode = nodeList.item(i);
                    if (null != currentNode && Node.TEXT_NODE == currentNode.getNodeType()
                            && "".equals(currentNode.getNodeValue().trim())) {
                        continue;
                    }
                    nodes.add(currentNode);

                }
                return nodes.toArray();
            }

            public Object getParent(Object element) {
                return ((Node) element).getParentNode();
            }

            public boolean hasChildren(Object element) {
                return ((Node) element).hasChildNodes();
            }

            public Object[] getElements(Object inputElement) {
                return getChildren(inputElement);
            }

            public void dispose() {
            }

            public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            }
        };
    }

    //Code von https://www.tutorials.de/threads/xml-file-als-tree-in-swt-darstellen.262425/
    //author Thomas.Darimont
    private Object createLazyTreeModelFrom(String xmlFilePath) {
        Node documentNode = null;
        try {
            documentNode = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(xmlFilePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return documentNode;
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

}