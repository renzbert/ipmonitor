package view;

import java.util.HashSet;
import java.util.Set;

import model.ModelObject;
import model.ObservedDownload;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.TableViewer;
import org.gudy.azureus2.plugins.disk.DiskManagerFileInfo;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;

public class FileList extends Dialog {
	private DataBindingContext m_bindingContext;
	private ObservedDownload download;
	private MyFiles files;
	private IObservableSet fl;
	private Table table;
	private TableViewer tableViewer;
	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public FileList(Shell parentShell, ObservedDownload downlaod) {
		super(parentShell);
		this.download=download;
		files=new MyFiles();
		fl= BeansObservables.observeSet(Realm.getDefault(), files, "files");
		for(DiskManagerFileInfo dmfi: downlaod.getDownload().getDiskManagerFileInfo()){
			files.getFiles().add(new MyFile(dmfi.getFile().getName(), !(dmfi.isSkipped())));
		}
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		
		TableColumn tblclmnDownload = new TableColumn(table, SWT.NONE);
		tblclmnDownload.setWidth(100);
		tblclmnDownload.setText("Download");
		
		TableColumn tblclmnFile = new TableColumn(table, SWT.NONE);
		tblclmnFile.setWidth(100);
		tblclmnFile.setText("File");

		return container;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		m_bindingContext = initDataBindings();
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 500);
	}
	
	public class MyFiles extends ModelObject{
		Set<MyFile> files;
		
		public MyFiles(){
			files= new HashSet<>();
		}
		
		public Set<MyFile> getFiles() {
			return files;
		}

		public void setFiles(Set<MyFile> files) {
			this.files = files;
		}
	}
	
	public class MyFile extends ModelObject{
		String name;
		boolean skipped;
		
		public MyFile(String s, boolean b){
			name=s;
			skipped=b;
		}
		
		public boolean isSkipped() {
			return skipped;
		}
		public void setSkipped(boolean skipped) {
			this.skipped = skipped;
		}
		public String getName() {
			return name;
		}
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		ObservableSetContentProvider setContentProvider = new ObservableSetContentProvider();
		IObservableMap[] observeMaps = BeansObservables.observeMaps(setContentProvider.getKnownElements(), MyFile.class, new String[]{"skipped", "name"});
		tableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMaps));
		tableViewer.setContentProvider(setContentProvider);
		//
		IObservableSet filesFilesObserveSet = BeanProperties.set("files").observe(files);
		tableViewer.setInput(filesFilesObserveSet);
		//
		return bindingContext;
	}
}
