package view;

import controller.IpMonitor;
import model.ObservedDownload;
import model.ObservedDownloads;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.plugins.PluginInterface;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class TargetTabFolder extends Composite {
    private DataBindingContext m_bindingContext;
    private Display display;
    private Shell shell;

    private IpMonitor ipm;
    private PluginInterface pi;
    private ObservedDownloads observedDownloads;
    private ObservedDownload myDownlaod;
    private List<TargetTab> tabs;

    /**
     * Create the composite.
     *
     * @param parent
     * @param style
     */
    public TargetTabFolder(Composite parent, int style, PluginInterface plugin_interface, IpMonitor ipMonitor) {
        super(parent, style);

        display = parent.getDisplay();
        shell = parent.getShell();
        this.ipm = ipMonitor;
        this.pi = plugin_interface;
        observedDownloads = ipm.getPeerFetcher().getObservedDownloads();

        setLayout(new FillLayout(SWT.HORIZONTAL));

        TabFolder tabFolder = new TabFolder(this, SWT.NONE);
        m_bindingContext = initDataBindings();

        tabs = new ArrayList<TargetTab>();
        observedDownloads.addPropertyChangeListener("downloads", new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                for (ObservedDownload myD : (List<ObservedDownload>) evt.getNewValue()) {
                    for (TargetTab tab : tabs) {
                        if (!tab.getDownload().getName().equals(myD.getName())) {
                            TabItem tabTargets = new TabItem(tabFolder, SWT.NONE);
                            TargetTab taT = new TargetTab(tabFolder, SWT.NONE, plugin_interface, ipm, myD);
                            tabTargets.setText(myD.getName());
                            tabTargets.setControl(taT);
                            tabs.add(taT);
                        }
                    }
                }
            }
        });

        List<ObservedDownload> downlaodList = new ArrayList<ObservedDownload>(observedDownloads.getObservedDownloads());
        for (ObservedDownload myD : downlaodList) {
            TabItem tabTargets = new TabItem(tabFolder, SWT.NONE);
            TargetTab taT = new TargetTab(tabFolder, SWT.NONE, plugin_interface, ipm, myD);
            tabTargets.setText(myD.getName());
            tabTargets.setControl(taT);
            tabs.add(taT);
        }
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        return bindingContext;
    }
}
