package view.evidence;
//Source mostly from http://www.java2s.com/Code/Java/SWT-JFace-Eclipse/DemonstratesTreeViewer.htm
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.swt.graphics.Image;

class FileTreeLabelProvider implements ILabelProvider {
	// The listeners
	private List<ILabelProviderListener> listeners;

	// Images for tree nodes
	private Image file;

	private Image dir;

	// Label provider state: preserve case of file names/directories
	boolean preserveCase;

	public FileTreeLabelProvider() {
		// Create the list to hold the listeners
		listeners = new ArrayList<ILabelProviderListener>();

		// Create the images
		try {
			file = new Image(null, new FileInputStream("images/file.png"));
			dir = new Image(null, new FileInputStream("images/dir.png"));
		} catch (FileNotFoundException e) {
			// Swallow it; we'll do without images
		}
	}

	public void setPreserveCase(boolean preserveCase) {
		this.preserveCase = preserveCase;

		// Since this attribute affects how the labels are computed,
		// notify all the listeners of the change.
		LabelProviderChangedEvent event = new LabelProviderChangedEvent(this);
		for (int i = 0, n = listeners.size(); i < n; i++) {
			ILabelProviderListener ilpl = (ILabelProviderListener) listeners.get(i);
			ilpl.labelProviderChanged(event);
		}
	}

	public Image getImage(Object arg0) {
		// If the node represents a directory, return the directory image.
		// Otherwise, return the file image.
		return ((File) arg0).isDirectory() ? dir : file;
	}

	public String getText(Object arg0) {
		// Get the name of the file
		String text = ((File) arg0).getName();

		// If name is blank, get the path
		if (text.length() == 0) {
			text = ((File) arg0).getPath();
		}

		// Check the case settings before returning the text
		return preserveCase ? text : text.toUpperCase();
	}

	public void addListener(ILabelProviderListener arg0) {
		listeners.add(arg0);
	}

	public void dispose() {
		// Dispose the images
		if (dir != null)
			dir.dispose();
		if (file != null)
			file.dispose();
	}

	public boolean isLabelProperty(Object arg0, String arg1) {
		return false;
	}

	public void removeListener(ILabelProviderListener arg0) {
		listeners.remove(arg0);
	}
}