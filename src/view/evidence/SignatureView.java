package view.evidence;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import controller.evidence.Security;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class SignatureView extends Composite {
	private Text text;
	private Text text_1;
	private Text text_2;

	String keyFile = "";
	String sigFile = "";
	String dataFile = "";

	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public SignatureView(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new FormLayout());

		Label lblSignatureFile = new Label(composite, SWT.NONE);
		FormData fd_lblSignatureFile = new FormData();
		fd_lblSignatureFile.top = new FormAttachment(0, 7);
		lblSignatureFile.setLayoutData(fd_lblSignatureFile);
		lblSignatureFile.setText("Signature file");

		Label lblPublickeyFile = new Label(composite, SWT.NONE);
		fd_lblSignatureFile.left = new FormAttachment(lblPublickeyFile, 0, SWT.LEFT);
		FormData fd_lblPublickeyFile = new FormData();
		fd_lblPublickeyFile.top = new FormAttachment(lblSignatureFile, 12);
		fd_lblPublickeyFile.left = new FormAttachment(0, 10);
		lblPublickeyFile.setLayoutData(fd_lblPublickeyFile);
		lblPublickeyFile.setText("Publickey file");

		text = new Text(composite, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.top = new FormAttachment(0, 4);
		fd_text.left = new FormAttachment(lblSignatureFile, 6);
		fd_text.right = new FormAttachment(100, -38);
		text.setLayoutData(fd_text);

		text_1 = new Text(composite, SWT.BORDER);
		FormData fd_text_1 = new FormData();
		fd_text_1.top = new FormAttachment(text, 6);
		fd_text_1.left = new FormAttachment(lblPublickeyFile, 6);
		text_1.setLayoutData(fd_text_1);

		text_2 = new Text(composite, SWT.BORDER);
		FormData fd_text_2 = new FormData();
		fd_text_2.right = new FormAttachment(100, -10);
		text_2.setLayoutData(fd_text_2);

		Label lblVerifies = new Label(composite, SWT.NONE);
		fd_text_2.left = new FormAttachment(lblVerifies, 6);
		fd_text_2.top = new FormAttachment(lblVerifies, -3, SWT.TOP);
		FormData fd_lblVerifies = new FormData();
		fd_lblVerifies.top = new FormAttachment(lblPublickeyFile, 12);
		fd_lblVerifies.right = new FormAttachment(lblSignatureFile, 0, SWT.RIGHT);
		lblVerifies.setLayoutData(fd_lblVerifies);
		lblVerifies.setText("verifies");

		Button chooseKey = new Button(composite, SWT.NONE);
		chooseKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dlg = new FileDialog(chooseKey.getShell(),  SWT.OPEN  );
				dlg.setText("Open");
				String path = dlg.open();
				if (path == null) return;
				text_1.setText(path);
			}
		});
		fd_text_1.right = new FormAttachment(chooseKey, -6);
		FormData fd_chooseKey = new FormData();
		fd_chooseKey.right = new FormAttachment(100, -10);
		fd_chooseKey.top = new FormAttachment(lblPublickeyFile, -5, SWT.TOP);
		chooseKey.setLayoutData(fd_chooseKey);
		chooseKey.setText("...");

		Button chooseSig = new Button(composite, SWT.NONE);
		chooseSig.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dlg = new FileDialog(chooseSig.getShell(),  SWT.OPEN  );
				dlg.setText("Open");
				String path = dlg.open();
				if (path == null) return;
				text.setText(path);
			}
		});
		chooseSig.setText("...");
		FormData fd_chooseSig = new FormData();
		fd_chooseSig.top = new FormAttachment(0);
		fd_chooseSig.right = new FormAttachment(text_2, 0, SWT.RIGHT);
		chooseSig.setLayoutData(fd_chooseSig);

		Button btnVerify = new Button(composite, SWT.NONE);
		btnVerify.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setData(text_1.getText(), text.getText(), dataFile);
			}
		});
		FormData fd_btnVerify = new FormData();
		fd_btnVerify.top = new FormAttachment(text_2, 12);
		fd_btnVerify.right = new FormAttachment(text, 0, SWT.RIGHT);
		btnVerify.setLayoutData(fd_btnVerify);
		btnVerify.setText("verify");

	}

	public void setData(String keyFile, String sigFile, String dataFile) {
		this.keyFile = keyFile;
		this.sigFile = sigFile;
		this.dataFile = dataFile;

		text.setText(sigFile);
		text_1.setText(keyFile);
		int verifies = Security.verSig(keyFile, sigFile, dataFile);
		switch (verifies) {
			case 0:
				text_2.setText("Signature does NOT match!");
				break;
			case 1:
				text_2.setText("OK");
				break;
			case -1:
				text_2.setText("Signature and/or Keyfile not found!");
				break;
			case -100:
				text_2.setText("Processing Signature and/or Keyfile failed!");
				break;
			default:
				text_2.setText("Something got completely wrong!");
				break;
		}

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
