package view.evidence;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.plugins.PluginInterface;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class EvidenceTab extends Composite {
    File history = null;
    File info = null;
    File messages = null;

    List<String> zipList = null;
    SignatureView compositeSig;

    /**
     * Create the composite.
     *
     * @param parent
     * @param style
     */
    public EvidenceTab(Composite parent, int style, PluginInterface pluginInterface) {
        super(parent, style);
        setLayout(new FillLayout(SWT.HORIZONTAL));

        SashForm sashForm = new SashForm(this, SWT.NONE);

        Composite composite = new Composite(sashForm, SWT.NONE);
        composite.setLayout(new FillLayout(SWT.HORIZONTAL));

        TreeViewer treeViewerFile = new TreeViewer(composite, SWT.BORDER);
        treeViewerFile.setContentProvider(new FileTreeContentProvider());
        treeViewerFile.setLabelProvider(new FileTreeLabelProvider());
        treeViewerFile.setInput("root");

        Tree treeFile = treeViewerFile.getTree();

        treeFile.setHeaderVisible(true);
        treeFile.setLinesVisible(true);

        Composite compositeEvidence = new Composite(sashForm, SWT.NONE);
        compositeEvidence.setLayout(new FillLayout(SWT.HORIZONTAL));

        SashForm sashForm_1 = new SashForm(compositeEvidence, SWT.VERTICAL);

        Composite compositeInfo = new Composite(sashForm_1, SWT.NONE);
        compositeInfo.setLayout(new FillLayout(SWT.HORIZONTAL));

        Group group = new Group(compositeInfo, SWT.NONE);
        group.setLayout(new FillLayout(SWT.HORIZONTAL));

        ListViewer listViewer = new ListViewer(group, SWT.BORDER | SWT.V_SCROLL);
        listViewer.setContentProvider(new IStructuredContentProvider() {
            @Override
            public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
            }

            @Override
            public void dispose() {
            }

            @SuppressWarnings("unchecked")
            @Override
            public Object[] getElements(Object arg0) {
                return ((List<String>) arg0).toArray();
            }
        });
        listViewer.setInput(zipList);

        compositeSig = new SignatureView(group, SWT.NONE);

        TabFolder tabFolder = new TabFolder(sashForm_1, SWT.NONE);

        TabItem tbtmInfoxml = new TabItem(tabFolder, SWT.NONE);
        tbtmInfoxml.setText("info.xml");

        Composite infoXml = new XmlView(tabFolder, SWT.NONE, info);
        tbtmInfoxml.setControl(infoXml);

        TabItem tbtmHistoryxml = new TabItem(tabFolder, SWT.NONE);
        tbtmHistoryxml.setText("history.xml");

        Composite historyXml = new XmlView(tabFolder, SWT.NONE, history);
        tbtmHistoryxml.setControl(historyXml);

        TabItem tbtmMessagexml = new TabItem(tabFolder, SWT.NONE);
        tbtmMessagexml.setText("messages.xml");

        Composite messagesXml = new XmlView(tabFolder, SWT.NONE, messages);
        tbtmMessagexml.setControl(messagesXml);
        sashForm_1.setWeights(new int[]{124, 264});
        sashForm.setWeights(new int[]{257, 548});
        addDisposeListener(e -> {

        });

        treeFile.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                TreeItem item = (TreeItem) e.item;
                File s = (File) item.getData();
                if (s.getName().toLowerCase().endsWith(".zip")) {
                    unzip(s.getAbsolutePath());
                    group.setText(s.getName());
                    listViewer.setInput(zipList);
                    if (info != null) {
                        ((XmlView) infoXml).setSource(info);
                    }
                    if (historyXml != null) {
                        ((XmlView) historyXml).setSource(history);
                    }

                    if (messagesXml != null) {
                        ((XmlView) messagesXml).setSource(messages);
                    }

                    String name = s.getAbsolutePath().substring(0, s.getAbsolutePath().length() - 4);
                    String publicKeyPath = pluginInterface.getPluginconfig().getPluginStringParameter("publicKeyFile");
                    compositeSig.setData(publicKeyPath, name + ".sig", s.getAbsolutePath());
                }
            }

        });
    }

    private void unzip(String source) {

        File tmp;
        try {
            ZipFile zipFile = new ZipFile(source);
            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            ZipInputStream zipInput;
            FileWriter wr;
            zipList = new ArrayList<>();
            while (entries.hasMoreElements()) {
                final ZipEntry zipEntry = entries.nextElement();
                if (!zipEntry.isDirectory()) {
                    final String fileName = zipEntry.getName();
                    if (fileName.endsWith(".xml")) {
                        tmp = File.createTempFile("tmp", "xml");
                        zipInput = new ZipInputStream(zipFile.getInputStream(zipEntry));
                        BufferedReader br = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry), "UTF-8"));
                        wr = new FileWriter(tmp);
                        String line;
                        while ((line = br.readLine()) != null) {
                            wr.write(line);
                        }
                        wr.close();
                        zipInput.close();
                        if (fileName.endsWith("info.xml")) {
                            info = tmp;
                        } else if (fileName.endsWith("history.xml")) {
                            history = tmp;
                        } else if (fileName.endsWith("messages.xml")) {
                            messages = tmp;
                        }
                    }
                    zipList.add(fileName);
                }
            }
            zipFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
