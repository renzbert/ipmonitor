package view;

import controller.IpMonitor;
import model.ObservedDownload;
import model.ObservedDownloads;
import model.ObservedPeer;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.download.DownloadListener;

public class TorrentTab extends Composite {
    private DataBindingContext m_bindingContext;
    private Display display;
    private Shell shell;

    private IpMonitor ipm;
    private PluginInterface pi;
    private ObservedDownloads observedDownloads;
    private ObservedDownload myDownlaod;
    private DownloadListener downloadListener;

    private Table tableDownloads;
    private TableViewer tableViewerDownloads;
    private Table tablePeers;
    private TableViewer tableViewerPeers;
    private Button btnCountryfilter;
    private Button btnBlacklist;
    private Button btnWhitelist;
    private Button btnEditCountries;
    private Button btnEditBlacklist;
    private Button btnEditWhitelist;
    private Button btnStart;
    private Button btnStop;
    private Label lblName;
    private Label lblStatus;
    private Label lblLocation;
    private Label lblDone;
    private Group grpTorretnInfo;
    private Group grpFilter;
    private Group grpControl;
    private TableColumn tblclmnDownloadState;
    private Button btnFileSelection;
    private TableColumn tblclmnCountry;
    private Text textName;
    private Text textStatus;
    private Text textLocation;
    private Text textDone;

    /**
     * Create the composite.
     *
     * @param parent
     * @param style
     * @param ipMonitor
     * @param plugin_interface
     */
    public TorrentTab(Composite parent, int style, PluginInterface plugin_interface, IpMonitor ipMonitor) {
        super(parent, style);
        downloadListener = new MyDownloadListener();
        display = parent.getDisplay();
        shell = parent.getShell();
        this.ipm = ipMonitor;
        this.pi = plugin_interface;
        observedDownloads = ipm.getPeerFetcher().getObservedDownloads();

        setLayout(new FillLayout(SWT.HORIZONTAL));

        SashForm sashForm = new SashForm(this, SWT.NONE);

        tableViewerDownloads = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
        tableViewerDownloads.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent arg0) {
                IStructuredSelection selection = (IStructuredSelection) tableViewerDownloads.getSelection();
                setMyDownload((ObservedDownload) selection.getFirstElement());
            }
        });
        tableDownloads = tableViewerDownloads.getTable();
        tableDownloads.setHeaderVisible(true);

        TableColumn tblclmnDownloads = new TableColumn(tableDownloads, SWT.NONE);
        tblclmnDownloads.setWidth(200);
        tblclmnDownloads.setText("Downloads");

        tblclmnDownloadState = new TableColumn(tableDownloads, SWT.NONE);
        tblclmnDownloadState.setWidth(100);
        tblclmnDownloadState.setText("State");

        tableViewerPeers = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
        tableViewerPeers.addDoubleClickListener(new IDoubleClickListener() {
            public void doubleClick(DoubleClickEvent e) {
                IStructuredSelection selection = (IStructuredSelection) e.getSelection();
                ObservedPeer myP = (ObservedPeer) selection.getFirstElement();
                myP.setTarget(!myP.isTarget());
            }
        });
        tablePeers = tableViewerPeers.getTable();
        tablePeers.setHeaderVisible(true);

        TableColumn tblclmnIp = new TableColumn(tablePeers, SWT.NONE);
        tblclmnIp.setWidth(100);
        tblclmnIp.setText("IP");

        TableColumn tblclmnPort = new TableColumn(tablePeers, SWT.NONE);
        tblclmnPort.setWidth(100);
        tblclmnPort.setText("Port");

        TableColumn tblclmnState = new TableColumn(tablePeers, SWT.NONE);
        tblclmnState.setWidth(100);
        tblclmnState.setText("State");

        tblclmnCountry = new TableColumn(tablePeers, SWT.NONE);
        tblclmnCountry.setWidth(100);
        tblclmnCountry.setText("Country");

        Composite composite = new Composite(sashForm, SWT.NONE);
        composite.setLayout(new FillLayout(SWT.VERTICAL));

        grpTorretnInfo = new Group(composite, SWT.NONE);
        grpTorretnInfo.setText("Torretn Info");
        grpTorretnInfo.setLayout(new GridLayout(2, false));

        lblName = new Label(grpTorretnInfo, SWT.NONE);
        lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblName.setText("Name");

        textName = new Text(grpTorretnInfo, SWT.BORDER);
        textName.setEditable(false);
        textName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        lblStatus = new Label(grpTorretnInfo, SWT.NONE);
        lblStatus.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblStatus.setText("Status");

        textStatus = new Text(grpTorretnInfo, SWT.BORDER);
        textStatus.setEditable(false);
        textStatus.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        lblLocation = new Label(grpTorretnInfo, SWT.NONE);
        lblLocation.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblLocation.setText("Location");

        textLocation = new Text(grpTorretnInfo, SWT.BORDER);
        textLocation.setEditable(false);
        textLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        lblDone = new Label(grpTorretnInfo, SWT.NONE);
        lblDone.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblDone.setText("Done");

        textDone = new Text(grpTorretnInfo, SWT.BORDER);
        textDone.setEditable(false);
        textDone.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        new Label(grpTorretnInfo, SWT.NONE);
        new Label(grpTorretnInfo, SWT.NONE);

        btnFileSelection = new Button(grpTorretnInfo, SWT.NONE);
        btnFileSelection.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                Realm.runWithDefault(SWTObservables.getRealm(composite.getDisplay()), new Runnable() {
                    public void run() {

                        try {
                            FileList fs = new FileList(shell, myDownlaod);
                            if (fs.open() == Window.OK) {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        btnFileSelection.setEnabled(false);
        btnFileSelection.setText("Filelist");
        new Label(grpTorretnInfo, SWT.NONE);

        grpFilter = new Group(composite, SWT.NONE);
        grpFilter.setText("Filter");
        grpFilter.setLayout(new FormLayout());

        btnCountryfilter = new Button(grpFilter, SWT.CHECK);
        btnCountryfilter.setEnabled(false);
        btnCountryfilter.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                myDownlaod.setCountry(btnCountryfilter.getSelection());

            }
        });
        FormData fd_btnCountryfilter = new FormData();
        fd_btnCountryfilter.top = new FormAttachment(0, 10);
        fd_btnCountryfilter.left = new FormAttachment(0, 10);
        btnCountryfilter.setLayoutData(fd_btnCountryfilter);
        btnCountryfilter.setText("Country-Filter");

        btnWhitelist = new Button(grpFilter, SWT.CHECK);
        btnWhitelist.setEnabled(false);
        btnWhitelist.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                myDownlaod.setWhite(btnWhitelist.getSelection());
            }
        });
        FormData fd_btnWhitelist = new FormData();
        fd_btnWhitelist.left = new FormAttachment(0, 10);
        btnWhitelist.setLayoutData(fd_btnWhitelist);
        btnWhitelist.setText("White-List");

        btnEditWhitelist = new Button(grpFilter, SWT.NONE);
        btnEditWhitelist.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                Realm.runWithDefault(SWTObservables.getRealm(composite.getDisplay()), new Runnable() {
                    public void run() {

                        try {
                            IpFilter fe = new IpFilter(shell, myDownlaod.getWhiteListFilter());
                            if (fe.open() == Window.OK) {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        btnEditWhitelist.setEnabled(false);
        FormData fd_btnEditWhitelist = new FormData();
        fd_btnEditWhitelist.right = new FormAttachment(btnWhitelist, 112, SWT.RIGHT);
        fd_btnEditWhitelist.left = new FormAttachment(btnWhitelist, 32);
        fd_btnEditWhitelist.top = new FormAttachment(btnWhitelist, -4, SWT.TOP);
        btnEditWhitelist.setLayoutData(fd_btnEditWhitelist);
        btnEditWhitelist.setText("Edit");

        btnEditBlacklist = new Button(grpFilter, SWT.NONE);
        btnEditBlacklist.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                Realm.runWithDefault(SWTObservables.getRealm(composite.getDisplay()), new Runnable() {
                    public void run() {

                        try {
                            IpFilter fe = new IpFilter(shell, myDownlaod.getBlackListFilter());
                            if (fe.open() == Window.OK) {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        btnEditBlacklist.setEnabled(false);
        FormData fd_btnEditBlacklist = new FormData();
        btnEditBlacklist.setLayoutData(fd_btnEditBlacklist);
        btnEditBlacklist.setText("Edit");

        btnBlacklist = new Button(grpFilter, SWT.CHECK);
        btnBlacklist.setEnabled(false);
        btnBlacklist.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                myDownlaod.setBlack(btnBlacklist.getSelection());
            }
        });
        fd_btnEditBlacklist.right = new FormAttachment(btnBlacklist, 115, SWT.RIGHT);
        fd_btnEditBlacklist.left = new FormAttachment(btnBlacklist, 35);
        fd_btnWhitelist.top = new FormAttachment(btnBlacklist, 21);
        fd_btnEditBlacklist.top = new FormAttachment(btnBlacklist, -4, SWT.TOP);
        FormData fd_btnBlacklist = new FormData();
        fd_btnBlacklist.top = new FormAttachment(btnCountryfilter, 20);
        fd_btnBlacklist.left = new FormAttachment(0, 10);
        btnBlacklist.setLayoutData(fd_btnBlacklist);
        btnBlacklist.setText("Black-List");

        btnEditCountries = new Button(grpFilter, SWT.NONE);
        btnEditCountries.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                Realm.runWithDefault(SWTObservables.getRealm(composite.getDisplay()), new Runnable() {
                    public void run() {

                        try {
                            CountryFilterEditor fe = new CountryFilterEditor(shell, myDownlaod.getCountryFilter());
                            if (fe.open() == Window.OK) {
                                for (ObservedPeer p : myDownlaod.getObservedPeers()) {// Filter
                                    // or
                                    // target
                                    // setting
                                    p.setTarget(myDownlaod.filter(p,
                                            pi.getPluginconfig().getPluginBooleanParameter("enableTor")));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        btnEditCountries.setEnabled(false);
        FormData fd_btnEditCountries = new FormData();
        fd_btnEditCountries.right = new FormAttachment(btnCountryfilter, 92, SWT.RIGHT);
        fd_btnEditCountries.left = new FormAttachment(btnCountryfilter, 12);
        fd_btnEditCountries.top = new FormAttachment(0, 6);
        btnEditCountries.setLayoutData(fd_btnEditCountries);
        btnEditCountries.setText("Edit");

        grpControl = new Group(composite, SWT.NONE);
        grpControl.setText("Control");
        grpControl.setLayout(new GridLayout(3, false));

        btnStart = new Button(grpControl, SWT.NONE);
        btnStart.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (myDownlaod != null) {
                    myDownlaod.getDownload().startDownload(true);

                }
            }
        });
        btnStart.setEnabled(false);
        btnStart.setText("Start");

        btnStop = new Button(grpControl, SWT.NONE);
        btnStop.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (myDownlaod != null) {
                    myDownlaod.getDownload().stopDownload();

                }
            }
        });
        btnStop.setEnabled(false);
        btnStop.setText("Stop");
        new Label(grpControl, SWT.NONE);
        sashForm.setWeights(new int[]{208, 311, 307});

        m_bindingContext = initDataBindings();

    }

    private void setMyDownload(ObservedDownload md) {
        if (md != null) {
            if (myDownlaod != null) {
                myDownlaod.getDownload().removeListener(downloadListener);
            }
            myDownlaod = md;
            myDownlaod.getDownload().addListener(downloadListener);
            updateTorrentInfo();
            updateFilter();
            updateControl();
        }
    }

    private void updateControl() {
        btnStart.setEnabled(true);
        btnStop.setEnabled(true);
        btnFileSelection.setEnabled(true);
    }

    private void updateFilter() {

        btnCountryfilter.setSelection(myDownlaod.isCountry());
        btnBlacklist.setSelection(myDownlaod.isBlack());
        btnWhitelist.setSelection(myDownlaod.isWhite());

        btnCountryfilter.setEnabled(true);
        btnBlacklist.setEnabled(true);
        btnWhitelist.setEnabled(true);

        btnEditCountries.setEnabled(true);
        btnEditBlacklist.setEnabled(true);
        btnEditWhitelist.setEnabled(true);
    }

    private void updateTorrentInfo() {
        textName.setText(myDownlaod.getName());
        textStatus.setText(myDownlaod.getDownload().getStats().getStatus());
        textLocation.setText(myDownlaod.getDownload().getStats().getDownloadDirectory());
        textDone.setText((myDownlaod.getDownload().getStats().getCompleted()) / 10 + "%");
        grpTorretnInfo.layout();
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

    public void delete() {
    }

    private class MyDownloadListener implements DownloadListener {

        @Override
        public void positionChanged(Download arg0, int arg1, int arg2) {
        }

        @Override
        public void stateChanged(Download arg0, int old_state, int new_state) {
            if (new_state == Download.ST_STOPPED) {
                observedDownloads.getDownload(arg0).removeAllPeers();
            }
            Display.getDefault().syncExec(new Runnable() {
                public void run() {
                    updateTorrentInfo();
                }
            });
        }
    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
        IObservableMap observeMap = BeansObservables.observeMap(listContentProvider.getKnownElements(),
                ObservedDownload.class, "name");
        tableViewerDownloads.setLabelProvider(new ObservableMapLabelProvider(observeMap));
        tableViewerDownloads.setContentProvider(listContentProvider);
        //
        IObservableList downloadsMyDObserveList = BeanProperties.list("observedDownloads").observe(observedDownloads);
        tableViewerDownloads.setInput(downloadsMyDObserveList);
        //
        ObservableListContentProvider listContentProvider_1 = new ObservableListContentProvider();
        IObservableMap[] observeMap_1 = BeansObservables.observeMaps(listContentProvider_1.getKnownElements(),
                ObservedPeer.class, new String[]{"ip", "port", "state", "countryCode"});
        tableViewerPeers.setLabelProvider(new ObservableMapLabelProvider(observeMap_1));
        tableViewerPeers.setContentProvider(listContentProvider_1);
        //
        IObservableValue observeSingleSelectionTableViewerDownloads = ViewerProperties.singleSelection()
                .observe(tableViewerDownloads);
        IObservableList tableViewerPeersObserveDetailList = BeanProperties.list(ObservedDownload.class, "observedPeers", ObservedPeer.class)
                .observeDetail(observeSingleSelectionTableViewerDownloads);
        tableViewerPeers.setInput(tableViewerPeersObserveDetailList);
        //
        IObservableValue observeSelectionBtnCountryfilterObserveWidget = WidgetProperties.selection()
                .observe(btnCountryfilter);
        IObservableValue countryMyDownlaodObserveValue = BeanProperties.value("country").observe(myDownlaod);
        bindingContext.bindValue(observeSelectionBtnCountryfilterObserveWidget, countryMyDownlaodObserveValue, null,
                null);
        //
        return bindingContext;
    }
}
