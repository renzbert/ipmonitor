package view;

import controller.IpMonitor;
import controller.Utility;
import controller.evidence.*;
import controller.evidence.Security;
import model.ObservedDownload;
import model.ObservedPeer;
import model.ReassembledFile;
import model.ReassembledPiece;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.logging.LoggerChannel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TargetTab extends Composite {
    private DataBindingContext m_bindingContext;

    private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
    private Display display;
    private Shell shell;
    private IpMonitor ipm;
    private PluginInterface pi;
    private ObservedDownload myDownlaod;
    private ObservedPeer target = null;
    private Table table;
    private TableViewer tableViewer;
    private ViewerFilter filterViewer;
    private Text textClient;
    private Text textIp;
    private Text textPort;
    private Text textStatus;
    private Text textPiece;
    private TargetPropertyChangeListener targetListener;
    private Text textAbsolute;
    private Spinner spinnerPercentage;
    private Button btnAutoExtractEvidence;

    /**
     * Create the composite.
     *
     * @param parent
     * @param style
     */
    public TargetTab(Composite parent, int style, PluginInterface plugin_interface, IpMonitor ipMonitor,
                     ObservedDownload myDownlaod) {
        super(parent, style);

        targetListener = new TargetPropertyChangeListener();

        addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                toolkit.dispose();
            }
        });
        toolkit.adapt(this);
        toolkit.paintBordersFor(this);

        display = parent.getDisplay();
        shell = parent.getShell();
        this.ipm = ipMonitor;
        this.pi = plugin_interface;
        this.myDownlaod = myDownlaod;
        setLayout(new FillLayout(SWT.HORIZONTAL));

        SashForm sashForm = new SashForm(this, SWT.NONE);
        toolkit.adapt(sashForm);
        toolkit.paintBordersFor(sashForm);

        tableViewer = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
        tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent arg0) {
                IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
                setMyPeer((ObservedPeer) selection.getFirstElement());
            }

        });
        table = tableViewer.getTable();
        table.setHeaderVisible(true);
        toolkit.paintBordersFor(table);

        filterViewer = new ViewerFilter() {

            @Override
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                ObservedPeer p = (ObservedPeer) element;
                return p.isTarget();
            }
        };
        tableViewer.addFilter(filterViewer);
        myDownlaod.addPropertyChangeListener("observedPeers", new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Display.getDefault().syncExec(new Runnable() {
                    public void run() {
                        tableViewer.refresh();
                    }
                });
            }
        });

        TableColumn tblclmnTarget = new TableColumn(table, SWT.NONE);
        tblclmnTarget.setWidth(100);
        tblclmnTarget.setText("Target");

        TableColumn tblclmnState = new TableColumn(table, SWT.NONE);
        tblclmnState.setWidth(100);
        tblclmnState.setText("State");

        TableColumn tblclmnPieces = new TableColumn(table, SWT.NONE);
        tblclmnPieces.setWidth(100);
        tblclmnPieces.setText("Pieces");

        TableColumn tblclmnAz = new TableColumn(table, SWT.NONE);
        tblclmnAz.setWidth(100);
        tblclmnAz.setText("AZ");

        SashForm sashForm_1 = new SashForm(sashForm, SWT.VERTICAL);
        toolkit.adapt(sashForm_1);
        toolkit.paintBordersFor(sashForm_1);

        Group grpInfo = new Group(sashForm_1, SWT.NONE);
        grpInfo.setText("Info");
        toolkit.adapt(grpInfo);
        toolkit.paintBordersFor(grpInfo);
        grpInfo.setLayout(new GridLayout(2, false));

        Label lblClient = new Label(grpInfo, SWT.NONE);
        lblClient.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblClient.setSize(31, 15);
        toolkit.adapt(lblClient, true, true);
        lblClient.setText("Client");

        textClient = new Text(grpInfo, SWT.BORDER);
        textClient.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        textClient.setSize(327, 21);
        textClient.setEditable(false);
        toolkit.adapt(textClient, true, true);

        Label lblIp = new Label(grpInfo, SWT.NONE);
        lblIp.setSize(10, 15);
        toolkit.adapt(lblIp, true, true);
        lblIp.setText("IP");

        textIp = new Text(grpInfo, SWT.BORDER);
        textIp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        textIp.setSize(327, 21);
        toolkit.adapt(textIp, true, true);

        Label lblPort = new Label(grpInfo, SWT.NONE);
        lblPort.setSize(22, 15);
        toolkit.adapt(lblPort, true, true);
        lblPort.setText("Port");

        textPort = new Text(grpInfo, SWT.BORDER);
        textPort.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        textPort.setSize(327, 21);
        toolkit.adapt(textPort, true, true);

        Label lblStatus = new Label(grpInfo, SWT.NONE);
        lblStatus.setSize(32, 15);
        toolkit.adapt(lblStatus, true, true);
        lblStatus.setText("Status");

        textStatus = new Text(grpInfo, SWT.BORDER);
        textStatus.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        textStatus.setSize(327, 21);
        toolkit.adapt(textStatus, true, true);

        Label lblPieces = new Label(grpInfo, SWT.NONE);
        lblPieces.setSize(33, 15);
        toolkit.adapt(lblPieces, true, true);
        lblPieces.setText("Pieces");

        textPiece = new Text(grpInfo, SWT.BORDER);
        GridData gd_textPiece = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_textPiece.widthHint = 452;
        textPiece.setLayoutData(gd_textPiece);
        textPiece.setSize(327, 21);
        toolkit.adapt(textPiece, true, true);

        Group grpExtractEvidence = new Group(sashForm_1, SWT.NONE);
        grpExtractEvidence.setText("Extract evidence");
        toolkit.adapt(grpExtractEvidence);
        toolkit.paintBordersFor(grpExtractEvidence);
        grpExtractEvidence.setLayout(new GridLayout(2, false));

        Label lblPercentage = new Label(grpExtractEvidence, SWT.NONE);
        lblPercentage.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblPercentage.setText("ReassembledPiece Percentage Limit");
        toolkit.adapt(lblPercentage, true, true);

        spinnerPercentage = new Spinner(grpExtractEvidence, SWT.BORDER);
        spinnerPercentage.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                long selection = spinnerPercentage.getSelection();
                long result = (myDownlaod.getDownload().getTorrent().getPieceCount() * selection) / 100;
                textAbsolute.setText(result + "/" + myDownlaod.getDownload().getTorrent().getPieceCount());
            }
        });
        spinnerPercentage.setSelection(10);
        toolkit.adapt(spinnerPercentage);
        toolkit.paintBordersFor(spinnerPercentage);

        Label lblAbsolute = new Label(grpExtractEvidence, SWT.NONE);
        toolkit.adapt(lblAbsolute, true, true);
        lblAbsolute.setText("Absolute ReassembledPiece Limit");

        textAbsolute = new Text(grpExtractEvidence, SWT.BORDER);
        textAbsolute.setEditable(false);
        textAbsolute.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        textAbsolute.setText(
                (myDownlaod.getDownload().getTorrent().getPieceCount() * spinnerPercentage.getSelection() / 100) + "/"
                        + myDownlaod.getDownload().getTorrent().getPieceCount());
        toolkit.adapt(textAbsolute, true, true);
        new Label(grpExtractEvidence, SWT.NONE);

        btnAutoExtractEvidence = new Button(grpExtractEvidence, SWT.CHECK);
        btnAutoExtractEvidence.setSelection(true);

        toolkit.adapt(btnAutoExtractEvidence, true, true);
        btnAutoExtractEvidence.setText("auto extract evidence");
        new Label(grpExtractEvidence, SWT.NONE);
        new Label(grpExtractEvidence, SWT.NONE);

        Button btnDoIt = new Button(grpExtractEvidence, SWT.NONE);
        btnDoIt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        btnDoIt.setSize(37, 25);
        btnDoIt.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {

                IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
                ObservedPeer p = ((ObservedPeer) selection.getFirstElement());
                // p.getDownlaod().stop();
                extractEvidence(p);
            }
        });
        toolkit.adapt(btnDoIt, true, true);
        btnDoIt.setText("Manual");
        new Label(grpExtractEvidence, SWT.NONE);
        sashForm_1.setWeights(new int[]{1, 1});
        sashForm.setWeights(new int[]{1, 2});
        m_bindingContext = initDataBindings();

        display.asyncExec(new Runnable() {
            public void run() {
                if (btnAutoExtractEvidence.getSelection()) {
                    List<ObservedPeer> targetList = new ArrayList(myDownlaod.getObservedPeers());
                    long limit = myDownlaod.getDownload().getTorrent().getPieceCount()
                            * spinnerPercentage.getSelection() / 100;
                    for (ObservedPeer mp : targetList) {
                        if (mp.isTarget() && mp.getPieceCount() >= limit) {
                            extractEvidence(mp);
                        }
                    }
                }
                display.timerExec(10000, this);
            }
        });
    }

    public ObservedDownload getDownload() {
        return myDownlaod;
    }

    private void setMyPeer(ObservedPeer firstElement) {
        if (firstElement != null) {

            if (targetListener != null && target != null) {
                target.removePropertyChangeListener(targetListener);
            }
            target = firstElement;
            if (target.getClient() != null) {
                textClient.setText(target.getClient());
            }
            textIp.setText(target.getIp());
            textPort.setText(target.getPort() + "");
            textStatus.setText(target.getState());
            textPiece.setText(target.getPieceCount() + "");
            target.addPropertyChangeListener(targetListener);
        } else {
            textClient.setText("");
            textIp.setText("");
            textPort.setText("");
            textStatus.setText("");
            textPiece.setText("");
        }
    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
        IObservableMap[] observeMaps = BeansObservables.observeMaps(listContentProvider.getKnownElements(),
                ObservedPeer.class, new String[]{"ip", "state", "pieceCount", "az"});
        tableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMaps));
        tableViewer.setContentProvider(listContentProvider);
        //
        IObservableList peersMyDownlaodObserveList = BeanProperties.list("observedPeers").observe(myDownlaod);
        tableViewer.setInput(peersMyDownlaodObserveList);
        //
        return bindingContext;
    }

    private void extractEvidence(ObservedPeer p) {
        p.setTarget(false);
        p.getPeer().getManager().removePeer(p.getPeer());

        Job job = new Job("Extract evidence") {
            protected IStatus run(IProgressMonitor monitor) {
                String path = pi.getPluginconfig().getPluginStringParameter("defaultEvidenceFolder") + File.separator + p.getTorrent().getName();

                File torrentDirectory = new File(String.valueOf(path));
                if (!torrentDirectory.exists()) {
                    torrentDirectory.mkdir();
                }

                String peerId = Utility.byteArray2Hex(p.getId());
                String peerIp = p.getIp();
                if (peerId != null && peerId.length() > 0) {
                    path = path + File.separator + peerId;
                } else {
                    path = path + File.separator + peerIp;
                }
                File targetDirectory = new File(String.valueOf(path));
                if (!targetDirectory.exists()) {
                    targetDirectory.mkdir();
                }

                try {
                    Map<Long, ReassembledPiece> pieceList = MessageReassembler.ReassembleMessages(p.getInMsgList(), p.getTorrent(),
                            p.isAz());
                    p.getTorrent().writeToFile(new File(path + File.separator + p.getTorrent().getName() + ".torrent"));

                    monitor.worked(20);
                    Map<String, ReassembledFile> fileList = MessageReassembler.ReassamblePieces(pieceList, p.getTorrent(), path);
                    monitor.worked(40);
                    MessageExtractor.extractMessages(p, path + File.separator + "messages.xml");
                    InfoExtractor.extractInfo(p, pieceList, fileList, path + File.separator + "info.xml");
                    monitor.worked(50);
                    HistoryExtractor.extractHistory(p.getInMsgList(), pieceList, p, path + File.separator + "history.xml");
                    monitor.worked(60);
                    String file = Packer.pack(path, p);
                    monitor.worked(80);
                    try {
                        /*KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
                        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
                        keyGen.initialize(1024, random);
                        KeyPair pair = keyGen.generateKeyPair();
                        PrivateKey priv = pair.getPrivate();
                        PublicKey pub = pair.getPublic();
                        */

                        String privateKeyFile = pi.getPluginconfig().getPluginStringParameter("privateKeyFile");
                        Security.genSig(path, file, privateKeyFile);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    monitor.worked(100);

                    syncWithUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return Status.OK_STATUS;

            }

            private void syncWithUI() {
                Display.getDefault().asyncExec(new Runnable() {
                    public void run() {
                        // MessageDialog.openInformation(getShell(), "message",
                        // "completed!");
                        LoggerChannel channel = pi.getLogger().getChannel("ipmonitor");
                        channel.logAlert(LoggerChannel.LT_INFORMATION, "Evidence extraction for target\n" + "IP: "
                                + p.getIp() + "id: " + p.getIp() + "\nhas completed.");
                    }
                });
            }
        };
        job.setUser(true);
        job.schedule();
    }

    private class TargetPropertyChangeListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Display.getDefault().syncExec(new Runnable() {
                public void run() {
                    switch (evt.getPropertyName()) {
                        case "client":
                            textClient.setText(evt.getNewValue().toString());
                            break;
                        case "ip":
                            textIp.setText(evt.getNewValue().toString());
                            break;
                        case "port":
                            textPort.setText(evt.getNewValue().toString());
                            break;
                        case "state":
                            textStatus.setText(evt.getNewValue().toString());
                            break;
                        case "pieceCount":
                            textPiece.setText(evt.getNewValue().toString());
                            break;
                    }
                }
            });

        }
    }
}
