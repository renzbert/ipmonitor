package view;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Text;

public class witheList extends Composite {

	private DataBindingContext m_bindingContext;
	private view.TorrentTab torrentTab;
	private Scale backgroundModeScale;
	private Scale borderWidthScale;
	private Button captureButton;
	private Button disposedButton;
	private Button dragDetectButton;
	private Button enabledButton;
	private Button focusControlButton;
	private Button layoutDeferredButton;
	private Scale orientationScale;
	private Button redrawButton;
	private Button reparentableButton;
	private Scale scrollbarsModeScale;
	private Scale styleScale;
	private Text toolTipTextText;
	private Button touchEnabledButton;
	private Button visibleButton;

	public witheList(Composite parent, int style, view.TorrentTab newTorrentTab) {
		this(parent, style);
		setTorrentTab(newTorrentTab);
	}

	public witheList(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		new Label(this, SWT.NONE).setText("BackgroundMode:");

		backgroundModeScale = new Scale(this, SWT.HORIZONTAL);
		backgroundModeScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("BorderWidth:");

		borderWidthScale = new Scale(this, SWT.HORIZONTAL);
		borderWidthScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Capture:");

		captureButton = new Button(this, SWT.CHECK);
		captureButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Disposed:");

		disposedButton = new Button(this, SWT.CHECK);
		disposedButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("DragDetect:");

		dragDetectButton = new Button(this, SWT.CHECK);
		dragDetectButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Enabled:");

		enabledButton = new Button(this, SWT.CHECK);
		enabledButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("FocusControl:");

		focusControlButton = new Button(this, SWT.CHECK);
		focusControlButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("LayoutDeferred:");

		layoutDeferredButton = new Button(this, SWT.CHECK);
		layoutDeferredButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Orientation:");

		orientationScale = new Scale(this, SWT.HORIZONTAL);
		orientationScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Redraw:");

		redrawButton = new Button(this, SWT.CHECK);
		redrawButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Reparentable:");

		reparentableButton = new Button(this, SWT.CHECK);
		reparentableButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("ScrollbarsMode:");

		scrollbarsModeScale = new Scale(this, SWT.HORIZONTAL);
		scrollbarsModeScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Style:");

		styleScale = new Scale(this, SWT.HORIZONTAL);
		styleScale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("ToolTipText:");

		toolTipTextText = new Text(this, SWT.BORDER | SWT.SINGLE);
		toolTipTextText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("TouchEnabled:");

		touchEnabledButton = new Button(this, SWT.CHECK);
		touchEnabledButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(this, SWT.NONE).setText("Visible:");

		visibleButton = new Button(this, SWT.CHECK);
		visibleButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		if (torrentTab != null) {
			m_bindingContext = initDataBindings();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private DataBindingContext initDataBindings() {
		IObservableValue backgroundModeObserveWidget = SWTObservables.observeSelection(backgroundModeScale);
		IObservableValue backgroundModeObserveValue = PojoObservables.observeValue(torrentTab, "backgroundMode");
		IObservableValue borderWidthObserveWidget = SWTObservables.observeSelection(borderWidthScale);
		IObservableValue borderWidthObserveValue = PojoObservables.observeValue(torrentTab, "borderWidth");
		IObservableValue captureObserveWidget = SWTObservables.observeSelection(captureButton);
		IObservableValue captureObserveValue = PojoObservables.observeValue(torrentTab, "capture");
		IObservableValue disposedObserveWidget = SWTObservables.observeSelection(disposedButton);
		IObservableValue disposedObserveValue = PojoObservables.observeValue(torrentTab, "disposed");
		IObservableValue dragDetectObserveWidget = SWTObservables.observeSelection(dragDetectButton);
		IObservableValue dragDetectObserveValue = PojoObservables.observeValue(torrentTab, "dragDetect");
		IObservableValue enabledObserveWidget = SWTObservables.observeSelection(enabledButton);
		IObservableValue enabledObserveValue = PojoObservables.observeValue(torrentTab, "enabled");
		IObservableValue focusControlObserveWidget = SWTObservables.observeSelection(focusControlButton);
		IObservableValue focusControlObserveValue = PojoObservables.observeValue(torrentTab, "focusControl");
		IObservableValue layoutDeferredObserveWidget = SWTObservables.observeSelection(layoutDeferredButton);
		IObservableValue layoutDeferredObserveValue = PojoObservables.observeValue(torrentTab, "layoutDeferred");
		IObservableValue orientationObserveWidget = SWTObservables.observeSelection(orientationScale);
		IObservableValue orientationObserveValue = PojoObservables.observeValue(torrentTab, "orientation");
		IObservableValue redrawObserveWidget = SWTObservables.observeSelection(redrawButton);
		IObservableValue redrawObserveValue = PojoObservables.observeValue(torrentTab, "redraw");
		IObservableValue reparentableObserveWidget = SWTObservables.observeSelection(reparentableButton);
		IObservableValue reparentableObserveValue = PojoObservables.observeValue(torrentTab, "reparentable");
		IObservableValue scrollbarsModeObserveWidget = SWTObservables.observeSelection(scrollbarsModeScale);
		IObservableValue scrollbarsModeObserveValue = PojoObservables.observeValue(torrentTab, "scrollbarsMode");
		IObservableValue styleObserveWidget = SWTObservables.observeSelection(styleScale);
		IObservableValue styleObserveValue = PojoObservables.observeValue(torrentTab, "style");
		IObservableValue toolTipTextObserveWidget = SWTObservables.observeText(toolTipTextText, SWT.Modify);
		IObservableValue toolTipTextObserveValue = PojoObservables.observeValue(torrentTab, "toolTipText");
		IObservableValue touchEnabledObserveWidget = SWTObservables.observeSelection(touchEnabledButton);
		IObservableValue touchEnabledObserveValue = PojoObservables.observeValue(torrentTab, "touchEnabled");
		IObservableValue visibleObserveWidget = SWTObservables.observeSelection(visibleButton);
		IObservableValue visibleObserveValue = PojoObservables.observeValue(torrentTab, "visible");
		//
		DataBindingContext bindingContext = new DataBindingContext();
		//
		bindingContext.bindValue(backgroundModeObserveWidget, backgroundModeObserveValue, null, null);
		bindingContext.bindValue(borderWidthObserveWidget, borderWidthObserveValue, null, null);
		bindingContext.bindValue(captureObserveWidget, captureObserveValue, null, null);
		bindingContext.bindValue(disposedObserveWidget, disposedObserveValue, null, null);
		bindingContext.bindValue(dragDetectObserveWidget, dragDetectObserveValue, null, null);
		bindingContext.bindValue(enabledObserveWidget, enabledObserveValue, null, null);
		bindingContext.bindValue(focusControlObserveWidget, focusControlObserveValue, null, null);
		bindingContext.bindValue(layoutDeferredObserveWidget, layoutDeferredObserveValue, null, null);
		bindingContext.bindValue(orientationObserveWidget, orientationObserveValue, null, null);
		bindingContext.bindValue(redrawObserveWidget, redrawObserveValue, null, null);
		bindingContext.bindValue(reparentableObserveWidget, reparentableObserveValue, null, null);
		bindingContext.bindValue(scrollbarsModeObserveWidget, scrollbarsModeObserveValue, null, null);
		bindingContext.bindValue(styleObserveWidget, styleObserveValue, null, null);
		bindingContext.bindValue(toolTipTextObserveWidget, toolTipTextObserveValue, null, null);
		bindingContext.bindValue(touchEnabledObserveWidget, touchEnabledObserveValue, null, null);
		bindingContext.bindValue(visibleObserveWidget, visibleObserveValue, null, null);
		//
		return bindingContext;
	}

	public view.TorrentTab getTorrentTab() {
		return torrentTab;
	}

	public void setTorrentTab(view.TorrentTab newTorrentTab) {
		setTorrentTab(newTorrentTab, true);
	}

	public void setTorrentTab(view.TorrentTab newTorrentTab, boolean update) {
		torrentTab = newTorrentTab;
		if (update) {
			if (m_bindingContext != null) {
				m_bindingContext.dispose();
				m_bindingContext = null;
			}
			if (torrentTab != null) {
				m_bindingContext = initDataBindings();
			}
		}
	}

}
