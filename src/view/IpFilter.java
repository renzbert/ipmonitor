package view;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;

public class IpFilter extends Dialog {
    private Pattern pattern;
    private List<String> listFilter;

    private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])" + "(\\/(\\d|[1-2]\\d|3[0-2]))?$";
    private Text ipInput;
    private Button buttonRemove;

    /**
     * Create the dialog.
     *
     * @param parentShell
     */
    public IpFilter(Shell parentShell, List<String> listFilter) {
        super(parentShell);
        pattern = Pattern.compile(IPADDRESS_PATTERN);
        this.listFilter = listFilter;
    }

    /**
     * Create contents of the dialog.
     *
     * @param parent
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(new FormLayout());

        ListViewer listViewer = new ListViewer(container, SWT.BORDER | SWT.V_SCROLL);
        listViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent arg0) {
                IStructuredSelection selection = (IStructuredSelection) listViewer.getSelection();
                buttonRemove.setEnabled(selection.iterator().hasNext());
            }
        });
        listViewer.setContentProvider(new IStructuredContentProvider() {

            @Override
            public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
            }

            @Override
            public void dispose() {
            }

            @Override
            public Object[] getElements(Object arg0) {
                return listFilter.toArray();
            }
        });
        listViewer.setInput(listFilter);
        org.eclipse.swt.widgets.List list = listViewer.getList();
        FormData fd_list = new FormData();
        fd_list.top = new FormAttachment(0, 6);
        fd_list.left = new FormAttachment(0, 10);
        list.setLayoutData(fd_list);

        Composite composite = new Composite(container, SWT.NONE);
        fd_list.right = new FormAttachment(0, 448);
        FormData fd_composite = new FormData();
        fd_composite.left = new FormAttachment(0, 10);
        fd_composite.right = new FormAttachment(100);
        fd_composite.bottom = new FormAttachment(100, -10);
        fd_composite.top = new FormAttachment(100, -61);
        composite.setLayoutData(fd_composite);

        ipInput = new Text(composite, SWT.BORDER);
        ipInput.setBounds(10, 10, 332, 26);

        Button btnAdd = new Button(composite, SWT.NONE);
        btnAdd.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                listFilter.add(ipInput.getText());
                listViewer.refresh();
            }
        });
        btnAdd.setEnabled(false);
        btnAdd.setBounds(348, 10, 90, 25);
        btnAdd.setText("Add");

        buttonRemove = new Button(container, SWT.NONE);
        buttonRemove.setEnabled(false);
        buttonRemove.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                IStructuredSelection selection = (IStructuredSelection) listViewer.getSelection();
                Iterator iter = selection.iterator();
                while (iter.hasNext()) {
                    String entry = (String) iter.next();
                    listFilter.remove(entry);
                }
                listViewer.refresh();
            }
        });
        fd_list.bottom = new FormAttachment(100, -98);
        buttonRemove.setText("Remove");
        FormData fd_buttonRemove = new FormData();
        fd_buttonRemove.top = new FormAttachment(list, 6);
        fd_buttonRemove.right = new FormAttachment(100, -10);
        fd_buttonRemove.left = new FormAttachment(100, -96);
        buttonRemove.setLayoutData(fd_buttonRemove);
        ipInput.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                if (pattern.matcher(ipInput.getText()).matches()) {
                    ipInput.setForeground(new Color(Display.getCurrent(), 0, 0, 0));
                    btnAdd.setEnabled(true);
                } else {
                    ipInput.setForeground(new Color(Display.getCurrent(), 255, 0, 0));
                    btnAdd.setEnabled(false);
                }

            }
        });
        return container;
    }

    /**
     * Create contents of the button bar.
     *
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override
    protected Point getInitialSize() {
        return new Point(464, 600);
    }
}
