package view;

import model.ModelObject;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CountryFilterEditor extends Dialog {
    private DataBindingContext m_bindingContext;
    private Shell shell;
    private List<String> list;
    private Table table;
    private String[] locales;
    private List<Country> countryList;
    private Button btnIncludeUnknownCountries;
    private String unknownCountry = "--";
    private boolean includeUnknownCountries = false;

    /**
     * Create the dialog.
     *
     * @param parentShell
     * @param list
     */
    public CountryFilterEditor(Shell parentShell, List<String> list) {
        super(parentShell);
        setShellStyle(SWT.DIALOG_TRIM);
        this.shell = parentShell;
        this.list = list;

        countryList = new ArrayList<>();
        locales = Locale.getISOCountries();
        for (String s : locales) {
            boolean skip = false;
            for (String str : list) {
                if (str.toLowerCase().trim().contains(s.toLowerCase())) {
                    countryList.add(new Country(true, s, (new Locale("", s).getDisplayCountry())));
                    skip = true;
                    break;
                }
                if (str.equals(unknownCountry)) {
                    includeUnknownCountries = true;
                }
            }
            if (!skip) {
                countryList.add(new Country(false, s, (new Locale("", s).getDisplayCountry())));
            }
        }

    }

    /**
     * Create contents of the dialog.
     *
     * @param parent
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(new FormLayout());

        TableViewer tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
        table = tableViewer.getTable();
        FormData fd_table = new FormData();
        fd_table.top = new FormAttachment(0);
        fd_table.left = new FormAttachment(0);
        fd_table.right = new FormAttachment(100);
        table.setLayoutData(fd_table);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.CHECK);
        tableViewerColumn.setEditingSupport(new EditingSupport(tableViewer) {

            @Override
            protected void setValue(Object element, Object value) {
                Country c = (Country) element;
                c.setSelected((boolean) value);
                tableViewer.update(element, null);
            }

            @Override
            protected Object getValue(Object element) {
                return ((Country) element).isSelected();
            }

            @Override
            protected CellEditor getCellEditor(Object arg0) {
                return new CheckboxCellEditor(null, SWT.CHECK);
            }

            @Override
            protected boolean canEdit(Object arg0) {
                return true;
            }
        });
        tableViewerColumn.setLabelProvider(new EmulatedNativeCheckBoxLabelProvider(tableViewer) {

            @Override
            protected boolean isChecked(Object element) {
                return ((Country) element).isSelected();
            }
        });
        TableColumn tblclmnSelected = tableViewerColumn.getColumn();
        tblclmnSelected.setWidth(20);
        tblclmnSelected.setText("");

        TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        tableViewerColumn_1.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                return ((Country) element).getIso();
            }
        });
        TableColumn tblclmnIso = tableViewerColumn_1.getColumn();
        tblclmnIso.setWidth(100);
        tblclmnIso.setText("ISO 3166-1");

        TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
        tableViewerColumn_2.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                return ((Country) element).getName();
            }
        });
        TableColumn tblclmnName = tableViewerColumn_2.getColumn();
        tblclmnName.setWidth(500);
        tblclmnName.setText("Name");

        Composite composite = new Composite(container, SWT.NONE);
        fd_table.bottom = new FormAttachment(composite, -6);
        composite.setLayout(new FormLayout());
        FormData fd_composite = new FormData();
        fd_composite.top = new FormAttachment(0, 449);
        fd_composite.bottom = new FormAttachment(0, 501);
        fd_composite.left = new FormAttachment(0);
        fd_composite.right = new FormAttachment(100);
        composite.setLayoutData(fd_composite);

        Button btnSelectAll = new Button(composite, SWT.NONE);
        btnSelectAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                for (Country c : countryList) {
                    c.setSelected(true);
                }
                tableViewerColumn.getViewer().refresh();
            }
        });
        FormData fd_btnSelectAll = new FormData();
        fd_btnSelectAll.bottom = new FormAttachment(100, -10);
        fd_btnSelectAll.left = new FormAttachment(0, 10);
        btnSelectAll.setLayoutData(fd_btnSelectAll);
        btnSelectAll.setText("Select all");

        Button btnSelectNone = new Button(composite, SWT.NONE);
        btnSelectNone.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                for (Country c : countryList) {
                    c.setSelected(false);
                }
                tableViewerColumn.getViewer().refresh();
            }
        });
        FormData fd_btnSelectNone = new FormData();
        fd_btnSelectNone.bottom = new FormAttachment(btnSelectAll, 0, SWT.BOTTOM);
        fd_btnSelectNone.left = new FormAttachment(btnSelectAll, 15);
        btnSelectNone.setLayoutData(fd_btnSelectNone);
        btnSelectNone.setText("Select none");

        btnIncludeUnknownCountries = new Button(composite, SWT.CHECK);
        FormData fd_btnIncludeUnknownCountries = new FormData();
        fd_btnIncludeUnknownCountries.top = new FormAttachment(btnSelectAll, 0, SWT.TOP);
        fd_btnIncludeUnknownCountries.left = new FormAttachment(btnSelectNone, 6);
        btnIncludeUnknownCountries.setLayoutData(fd_btnIncludeUnknownCountries);
        btnIncludeUnknownCountries.setText("include unknown countries");
        btnIncludeUnknownCountries.setSelection(includeUnknownCountries);
        btnIncludeUnknownCountries.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent selectionEvent) {
                includeUnknownCountries = btnIncludeUnknownCountries.getSelection();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent selectionEvent) {

            }
        });

        tableViewer.setContentProvider(new ArrayContentProvider());
        tableViewer.setInput(countryList);

        return container;
    }

    /**
     * Create contents of the button bar.
     *
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        Button button = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                list.clear();
                for (Country c : countryList) {
                    if (c.isSelected()) {
                        list.add(c.getIso());
                    }
                }
                if (includeUnknownCountries) {
                    list.add(unknownCountry);
                }
            }
        });
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        m_bindingContext = initDataBindings();
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override
    protected Point getInitialSize() {
        return new Point(630, 600);
    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        return bindingContext;
    }

    private class Country extends ModelObject {
        private boolean selected;
        private String iso;
        private String name;

        public Country(boolean selected, String iso, String name) {
            super();
            this.selected = selected;
            this.iso = iso;
            this.name = name;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public String getIso() {
            return iso;
        }

        public void setIso(String iso) {
            this.iso = iso;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
